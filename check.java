package com.teyaar.fragement.TeyaarSocialFragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.litho.Column;
import com.facebook.litho.ComponentContext;
import com.facebook.litho.widget.GridLayoutInfo;
import com.facebook.litho.widget.Image;
import com.facebook.litho.widget.RecyclerBinder;
import com.facebook.litho.widget.Text;
import com.facebook.soloader.SoLoader;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.teyaar.R;
import com.teyaar.Reactions.ClickAction;
import com.teyaar.Reactions.ReactionView;
import com.teyaar.Utilities.Constants;
import com.teyaar.Utilities.CustomScrollListener;
import com.teyaar.Utilities.SpeedyLinearLayoutManager;
import com.teyaar.Utilities.TimeZoneConv;
import com.teyaar.Utilities.Utils;
//import com.teyaar.activity.TeyaarSocial.SocialCommnetsActivity;
import com.teyaar.activity.TeyaarSocial.SocialCommentActivityNew;
import com.teyaar.activity.TeyaarSocial.TeyaarHomeActivity;
import com.teyaar.adapter.SocilaAdapter.ReactionListAdapter;
import com.teyaar.adapter.SocilaAdapter.SocialPostsGridContentsAdapter;
import com.teyaar.adapter.SocilaAdapter.SocialPostsNewAdapter;
import com.teyaar.adapter.SocilaAdapter.SocialStoreisHome;
import com.teyaar.adapter.SocilaAdapter.SocialStoreisHomeNewAdapter;
import com.teyaar.agora.openlive.model.ConstantApp;
import com.teyaar.agora.openlive.ui.LiveRoomActivity;
import com.teyaar.base.BaseFragment;
import com.teyaar.interfaces.SocialInterface.SocialHomeInterface;
import com.teyaar.interfaces.SocialInterface.SocialStoresInterface;
import com.teyaar.model.BaseModel;
import com.teyaar.model.ChannelGetModel;
import com.teyaar.model.TeyaarSocialModel.MainDetails;
import com.teyaar.model.TeyaarSocialModel.SocialBroadcastModel;
import com.teyaar.model.TeyaarSocialModel.SocialPostContentsModel;
import com.teyaar.model.TeyaarSocialModel.SocialPostCountersModel;
import com.teyaar.model.TeyaarSocialModel.SocialPostsModel;
import com.teyaar.model.TeyaarSocialModel.SocialUserDetailsModel;
import com.teyaar.model.TeyaarSocialModel.SocialUserFriendsBroadcastModel;
import com.teyaar.model.TeyaarSocialModel.SocialUserPostsModel;
import com.teyaar.model.TeyaarSocialModel.newDataBean;
import com.teyaar.widget.OverlapDecoration;

import net.cachapa.expandablelayout.ExpandableLayout;
import net.idik.lib.slimadapter.SlimAdapter;
import net.idik.lib.slimadapter.SlimInjector;
import net.idik.lib.slimadapter.viewinjector.IViewInjector;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;
import static com.teyaar.Utilities.Constants.GOOGLELOCATIONAPIKEY;
import static com.teyaar.Utilities.Constants.SHAREBASEURL;

public class SocialHomeFragment extends BaseFragment implements SocialHomeInterface, SocialStoresInterface {
    private boolean loading = true;
    int visibleItemCount = 0;
    int totalItemCount = 0;
    Activity mActivity;
    int pastVisiblesItems = 0;
    int current_playing_position = -1;
    String current_playing_post_id = null;
    boolean isLoactionClicked = false;
    RecyclerView stories, socialPost;
    SocialStoreisHome mStoriesAdapter;
    int now_playing = 0;
    SocialPostsNewAdapter mSocialPostsNewAdapter;
    TextView addPost;
    SocialStoresInterface anInterface;
    ImageView prof_pic;
    FirebaseDatabase database;
    DatabaseReference myRef;
    ArrayList<SocialPostContentsModel.DataBean> list;
    String DataTableUserPost = "SocialUserPosts";
    String UserId = "";
    String DataTablePost = "SocialPosts";
    String DataTableUserDetails = "user_details";
    String DataTableUserStores = "SocialUserFriendsBroadcast";
    String DataTablePostComments = "SocialPostComments";
    String DataTablePostContents = "SocialPostContents";
    List<SocialUserPostsModel> userPostsList;
    List<SocialPostsModel> postList;
    List<SocialUserDetailsModel> UserDetailsList;
    List<SocialUserFriendsBroadcastModel> Stores_List = new ArrayList<>();
    private FirebaseAuth mAuth;
    int nowplaying = -1;
    SocialHomeInterface homeInterface;
    SocialHomeInterface Interface;
    ProgressBar progressBar;
    LinearLayoutManager layoutManager;
    //    NestedScrollView main_layout;

    ExpandableLayout contstraint;
    String lastID = "";
    String StoreStart;
    ImageView img_search;

    SocialStoreisHomeNewAdapter mSocialStoreisHomeNewAdapter;
    String postionReport = "0";
    BottomSheetBehavior bottomSheetBehavior;
    LinearLayout report_nudity, report_violence, report_harrasment, layout_search;
    LinearLayout report_reportsuicide, report_false, report_spam, report_else;
    LinearLayout report_speech, report_sale, report_terrorism, report_info;
    Display mDisplay;//= activty.getWindowManager().getDefaultDisplay();

    int device_height;// = mDisplay.getHeight();
    private static final int PICK_LOCATION_REQUEST = 200;
    private static final int PERMISSION_LOCATION_REQUEST = 9;
    List<Object> slimedata;
    RecyclerView rv2;
    SlimAdapter mSlimAdapter;
    CustomScrollListener mCustomScrollListener;

    // boolean loadfinished=false;
    Long calculateTime(Date endDate) {
        Date c = (Date) Calendar.getInstance().getTime();
        long duration = c.getTime() - endDate.getTime();
        Log.e(TAG, "calculateTime: " + duration);
        return duration;
    }


    void sethight2(List<newDataBean> postcontent, VideoView mTextureVideoView, FrameLayout f1) {
        Double original_width, original_height;

        if (postcontent.get(0).getImageHeight() != null) {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            // int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            Log.i("imageheight", "is " + postcontent.get(0).getImageHeight());
            original_height = Double.parseDouble(postcontent.get(0).getImageHeight() + "");
            original_width = Double.parseDouble(postcontent.get(0).getImageWidth());
            Double ration = original_height / original_width;
            Log.i("checking_23", ration + "");
            Double newh = width * ration;
            f1.getLayoutParams().width = width;
            f1.getLayoutParams().height = newh.intValue();
            f1.requestLayout();
            mTextureVideoView.getLayoutParams().width = width;
            mTextureVideoView.getLayoutParams().height = newh.intValue();
            mTextureVideoView.requestLayout();


            Log.i("imageheight197", "is not null w=>" + width + ":h=>" + newh + "   h=>" + original_height + ":w=>" + original_width);
            //  holder.video_layout.getLayoutParams().width=width;
            ///  holder.video_layout.getLayoutParams().height=newh.intValue();
            // holder.video_layout.requestLayout();
        } else {
            Log.i("imageheight197", "is null");

        }
    }

    void sethight3(List<newDataBean> postcontent, ImageView mTextureVideoView, FrameLayout f1) {
        Double original_width, original_height;

        if (postcontent.get(0).getImageHeight() != null) {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            // int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            Log.i("imageheight", "is " + postcontent.get(0).getImageHeight());
            original_height = Double.parseDouble(postcontent.get(0).getImageHeight() + "");
            original_width = Double.parseDouble(postcontent.get(0).getImageWidth());
            Double ration = original_height / original_width;
            Log.i("checking_23", ration + "");
            Double newh = width * ration;
            mTextureVideoView.getLayoutParams().width = width;
            mTextureVideoView.getLayoutParams().height = newh.intValue();
            mTextureVideoView.requestLayout();

            f1.getLayoutParams().width = width;
            f1.getLayoutParams().height = newh.intValue();
            f1.requestLayout();
            Log.i("imageheight197", "is not null w=>" + width + ":h=>" + newh + "   h=>" + original_height + ":w=>" + original_width);
            //  holder.video_layout.getLayoutParams().width=width;
            ///  holder.video_layout.getLayoutParams().height=newh.intValue();
            // holder.video_layout.requestLayout();
        } else {
            Log.i("imageheight197", "is null");

        }
    }

    void sethight(List<newDataBean> postcontent, VideoView mTextureVideoView, int pos) {
        Double original_width, original_height;

        if (postcontent.get(0).getImageHeight() != null) {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            // int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            Log.i("imageheight", "is " + postcontent.get(pos).getImageHeight());
            original_height = Double.parseDouble(postcontent.get(pos).getImageHeight() + "");
            original_width = Double.parseDouble(postcontent.get(pos).getImageWidth());
            Double ration = original_height / original_width;
            Log.i("checking_23", ration + "");
            Double newh = width * ration;
            mTextureVideoView.getLayoutParams().width = width;
            mTextureVideoView.getLayoutParams().height = newh.intValue();
            mTextureVideoView.requestLayout();
            Log.i("imageheight197", "is not null w=>" + width + ":h=>" + newh + "   h=>" + original_height + ":w=>" + original_width);
            //  holder.video_layout.getLayoutParams().width=width;
            ///  holder.video_layout.getLayoutParams().height=newh.intValue();
            // holder.video_layout.requestLayout();
        } else {
            Log.i("imageheight197", "is null");

        }
    }

    void sethight(List<newDataBean> postcontent, ImageView mTextureVideoView, int pos) {
        Double original_width, original_height;

        if (postcontent.get(0).getImageHeight() != null) {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            // int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            Log.i("imageheight", "is " + postcontent.get(pos).getImageHeight());
            original_height = Double.parseDouble(postcontent.get(pos).getImageHeight() + "");
            original_width = Double.parseDouble(postcontent.get(pos).getImageWidth());
            Double ration = original_height / original_width;
            Log.i("checking_23", ration + "");
            Double newh = width * ration;
            mTextureVideoView.getLayoutParams().width = width;
            mTextureVideoView.getLayoutParams().height = newh.intValue();
            mTextureVideoView.requestLayout();
            Log.i("imageheight197", "is not null w=>" + width + ":h=>" + newh + "   h=>" + original_height + ":w=>" + original_width);
            //  holder.video_layout.getLayoutParams().width=width;
            ///  holder.video_layout.getLayoutParams().height=newh.intValue();
            // holder.video_layout.requestLayout();
        } else {
            Log.i("imageheight197", "is null");

        }
    }

    void sethight(List<newDataBean> postcontent, PlayerView mTextureVideoView) {
        Double original_width, original_height;

        if (postcontent.get(0).getImageHeight() != null) {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            // int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            Log.i("imageheight", "is " + postcontent.get(0).getImageHeight());
            original_height = Double.parseDouble(postcontent.get(0).getImageHeight() + "");
            original_width = Double.parseDouble(postcontent.get(0).getImageWidth());
            Double ration = original_height / original_width;
            Log.i("checking_23", ration + "");
            Double newh = width * ration;
            mTextureVideoView.getLayoutParams().width = width;
            mTextureVideoView.getLayoutParams().height = newh.intValue();
            mTextureVideoView.requestLayout();
            Log.i("imageheight197", "is not null w=>" + width + ":h=>" + newh + "   h=>" + original_height + ":w=>" + original_width);
            //  holder.video_layout.getLayoutParams().width=width;
            ///  holder.video_layout.getLayoutParams().height=newh.intValue();
            // holder.video_layout.requestLayout();
        } else {
            Log.i("imageheight197", "is null");

        }
    }

    void sethight(List<newDataBean> postcontent, VideoView mTextureVideoView, ImageView top, FrameLayout frame) {
        Double original_width, original_height;

        if (postcontent.get(0).getImageHeight() != null) {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            // int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            Log.i("imageheight", "is " + postcontent.get(0).getImageHeight());
            original_height = Double.parseDouble(postcontent.get(0).getImageHeight() + "");
            original_width = Double.parseDouble(postcontent.get(0).getImageWidth());
            Double ration = original_height / original_width;
            Log.i("checking_23", ration + "");
            Double newh = width * ration;
            frame.getLayoutParams().width = width;
            frame.getLayoutParams().height = newh.intValue();
            frame.requestLayout();
            mTextureVideoView.getLayoutParams().width = width;
            mTextureVideoView.getLayoutParams().height = newh.intValue();
            mTextureVideoView.requestLayout();
            top.getLayoutParams().width = width;
            top.getLayoutParams().height = newh.intValue();
            top.requestLayout();
            Log.i("imageheight197", "is not null w=>" + width + ":h=>" + newh + "   h=>" + original_height + ":w=>" + original_width);
            //  holder.video_layout.getLayoutParams().width=width;
            ///  holder.video_layout.getLayoutParams().height=newh.intValue();
            // holder.video_layout.requestLayout();
        } else {
            Log.i("imageheight197", "is null");

        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Log.i("SocialHomeFragment", "called");
        mDisplay = getActivity().getWindowManager().getDefaultDisplay();
        mActivity = getActivity();
        device_height = mDisplay.getHeight();
        if (getActivity() != null) {
            if (view == null) {
                view = inflater.inflate(R.layout.social_home_fragment, container, false);
                database = FirebaseDatabase.getInstance();
                myRef = database.getReference();
                mAuth = FirebaseAuth.getInstance();
//              UserId ="-LTRr0Y_tMaOpibp8TV9";
                UserId = Constants.FCM_USER_ID;
            }

        }
        // ImageView imageView=(ImageView)view.findViewById(R.id.);
        rv2 = (RecyclerView) view.findViewById(R.id.rv2);
        SoLoader.init(getActivity(), false);
         ComponentContext context = new ComponentContext(getActivity());
         RecyclerBinder recyclerBinder = new RecyclerBinder.Builder()
                .layoutInfo(new GridLayoutInfo(getContext(), 2))
                .build(context);
        recyclerBinder.bind(rv2);

        //recyclerBinder.setCanMeasure(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1);
        SpeedyLinearLayoutManager l = new SpeedyLinearLayoutManager(getContext(), SpeedyLinearLayoutManager.VERTICAL, false);
//        LinearLayout bottomSheetLayout = getActivity().findViewById(R.id.linear_layout_bottom_sheet);
//        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetLayout);
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        // rv2.setLayoutManager(l);
        rv2.setLayoutManager(l);
        mSlimAdapter = SlimAdapter.create()
                .register(R.layout.spinner_social_home_page, new SlimInjector<ProgressSpinnerMilla>() {

                    @Override
                    public void onInject(ProgressSpinnerMilla data, IViewInjector injector) {

                    }
                })
                .register(R.layout.adapter_social_posts, new SlimInjector<MainDetails>() {
                    @Override
                    public void onInject(MainDetails data, IViewInjector iview) {

                        List<SocialPostCountersModel> postCountersModelList = new ArrayList<>();
                        final Bundle[] b = new Bundle[1];
                        final Fragment[] frag = {null};

                        ImageView viewviewoverlay;
                        FrameLayout millavideoplayer;
                        Context context = getContext();
                        data.setMultiple(false);
                        ClickAction actionClick;
                        RelativeLayout emojiesDialog;
                        ReactionListAdapter reactionListAdapter;
                        TextView txt_time, txt_like_count, txt_comment, txt_share, txt_title, txt_content, reactionCount, commentCount;
                        ImageView ProfileImage, reactionIcon;
                        RecyclerView icons;
                        RecyclerView grid;
                        View v3, v6, v5;
                        ImageView img1, img2, img3, img4, img5, img6;
                        LinearLayout commLayout, more_menu_layout, click_like, like_comment_Lyt;

                        TextView txt_location_view;
                        final ReactionView[] rvl = {null};//new ReactionView();

                        SocialPostsGridContentsAdapter adapter;
//                        LinearLayout like, angry, cry, happy, wow, love,
                        LinearLayout pastMainLayout, shareLayout, layout_location_view;

                        // data.buffer_video = (TextView) view.findViewById(R.id.buffer_video);
                        // data.buffer_video.setTextColor(getActivity().getResources().getColor(R.color.red));
                        data.custom_video_viewImageview = (ImageView) iview.findViewById(R.id.custom_video_view_imageView);

                        millavideoplayer = (FrameLayout) iview.findViewById(R.id.custom_video_view_frame);
                        v3 = (View) iview.findViewById(R.id.layout3images);
                        v3.setVisibility(View.GONE);
                        v6 = (View) iview.findViewById(R.id.layout6images);
                        v6.setVisibility(View.GONE);
                        v5 = (View) iview.findViewById(R.id.layout5images);
                        v5.setVisibility(View.GONE);


                        //  millavideoplayer.setVisibility(View.GONE);
                        viewviewoverlay = (ImageView) iview.findViewById(R.id.viewviewoverlay);

                        layout_location_view = (LinearLayout) iview.findViewById(R.id.layout_location_view);
                        txt_location_view = (TextView) iview.findViewById(R.id.txt_location_view);
                        commLayout = (LinearLayout) iview.findViewById(R.id.social_layout_comments);
                        click_like = (LinearLayout) iview.findViewById(R.id.click_like);
                        txt_time = (TextView) iview.findViewById(R.id.txt_date);
                        commentCount = (TextView) iview.findViewById(R.id.comment_count);
                        txt_like_count = (TextView) iview.findViewById(R.id.txt_like_count);
                        txt_comment = (TextView) iview.findViewById(R.id.txt_comment);
                        txt_share = (TextView) iview.findViewById(R.id.txt_share);
                        txt_title = (TextView) iview.findViewById(R.id.txt_title);
                        ProfileImage = (ImageView) iview.findViewById(R.id.imgUser);
                        txt_content = (TextView) iview.findViewById(R.id.txt_content);

                        grid = (RecyclerView) iview.findViewById(R.id.gridview);
                        more_menu_layout = (LinearLayout) iview.findViewById(R.id.more_menu_layout);

                        like_comment_Lyt = (LinearLayout) iview.findViewById(R.id.comment_reaction_layout);
                        reactionCount = (TextView) iview.findViewById(R.id.reaction_count);
                        reactionIcon = (ImageView) iview.findViewById(R.id.reaction_icon);
                        icons = (RecyclerView) iview.findViewById(R.id.reaction_list_recycler);

                        emojiesDialog = (RelativeLayout) iview.findViewById(R.id.emojies_layout);
//                        like = (LinearLayout) iview.findViewById(R.id.lyt_like_button);
//                        angry = (LinearLayout) iview.findViewById(R.id.lyt_angry_button);
//                        cry = (LinearLayout) iview.findViewById(R.id.lyt_cry_button);
//                        happy = (LinearLayout) iview.findViewById(R.id.lyt_happy_button);
//                        wow = (LinearLayout) iview.findViewById(R.id.lyt_wow_button);
//                        love = (LinearLayout) iview.findViewById(R.id.lyt_love_button);
                        pastMainLayout = (LinearLayout) iview.findViewById(R.id.past_main_layout);
                        shareLayout = (LinearLayout) iview.findViewById(R.id.click_share);

                        int position = data.getI();


                        if (data.getmSocialPostsModel().getLocation() != null) {
                            if (!data.getmSocialPostsModel().getLocation().equals("")) {
                                layout_location_view.setVisibility(View.VISIBLE);
                                txt_location_view.setText(data.getmSocialPostsModel().getLocation());
                            } else {
                                layout_location_view.setVisibility(View.GONE);
                            }
                        } else {
                            layout_location_view.setVisibility(View.GONE);
                        }


                        if (data.getmSocialUserDetailsModel() != null && (data.getmSocialUserDetailsModel().getUser_name() != null) ? true : false) {
                            txt_title.setText(data.getmSocialUserDetailsModel().getUser_name() + "");
                            RequestOptions options = new RequestOptions().centerCrop();
                            Glide.with(getContext()).load(data.getmSocialUserDetailsModel().getImage()).apply(options)
                                    .into(ProfileImage);
                        }

                        if (data.getmSocialPostContentsModelsList() != null ?
                                (data.getmSocialPostContentsModelsList().size() > 0 ? true : false) : false) {
                            //millavideoplayer.setVisibility(View.GONE);

                            if (data.getmSocialPostContentsModelsList().size() > 1) {
                                millavideoplayer.setVisibility(View.GONE);

                                // millavideoplayer.setVisibility(View.GONE);
                                data.setMultiple(true);
                                StaggeredGridLayoutManager GPLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                                grid.setLayoutManager(GPLayoutManager);
                                grid.setItemAnimator(new DefaultItemAnimator());

                                adapter = new SocialPostsGridContentsAdapter(getContext(), data.getmSocialPostContentsModelsList(), data.getPostid(), getSocialHomeActivity(), grid, data.getmSocialPostsModel().getUserID());
                                grid.setAdapter(adapter);
                                grid.setVisibility(View.VISIBLE);
                                // TODO: 23/1/19 check here

                                if (false || data.getmSocialPostContentsModelsList().size() == 5 || data.getmSocialPostContentsModelsList().size() == 3 || data.getmSocialPostContentsModelsList().size() == 6) {


                                    if (data.getmSocialPostContentsModelsList().size() == 3) {
                                        Log.i("image3", new Gson().toJson(data.getmSocialPostContentsModelsList().get(0)));
                                        v3.setVisibility(View.VISIBLE);
                                        v6.setVisibility(View.GONE);
                                        v5.setVisibility(View.GONE);
                                        data.mImg1 = (ImageView) iview.findViewById(R.id.layout3images).findViewById(R.id.imageview_newdesin_1);
                                        data.mImg2 = (ImageView) iview.findViewById(R.id.layout3images).findViewById(R.id.imageview_newdesin_2);
                                        data.mImg3 = (ImageView) iview.findViewById(R.id.layout3images).findViewById(R.id.imageview_newdesin_3);
                                        setclick(data.mImg1, 0, data);
                                        setclick(data.mImg2, 1, data);
                                        setclick(data.mImg3, 2, data);
                                        data.mImg1 = (ImageView) iview.findViewById(R.id.layout3images).findViewById(R.id.imageview_newdesin_1);
                                        data.mImg2 = (ImageView) iview.findViewById(R.id.layout3images).findViewById(R.id.imageview_newdesin_2);
                                        data.mImg3 = (ImageView) iview.findViewById(R.id.layout3images).findViewById(R.id.imageview_newdesin_3);
                                        setclick(data.mImg1, 0, data);
                                        setclick(data.mImg2, 1, data);
                                        setclick(data.mImg3, 2, data);
                                        {
                                            data.mImg1.setVisibility(View.VISIBLE);

                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).into(data.mImg1);
                                            //  Picasso.get().load(data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).into(data.mImg1);

                                        }

                                        {
                                            data.mImg2.setVisibility(View.VISIBLE);

                                            //  Picasso.get().load(data.getmSocialPostContentsModelsList().get(1).getCdnUrl()).into(data.mImg2);
                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(1).getCdnUrl()).into(data.mImg2);
                                        }

                                        {
                                            data.mImg3.setVisibility(View.VISIBLE);

                                            // Picasso.get().load(data.getmSocialPostContentsModelsList().get(2).getCdnUrl()).into(data.mImg3);

                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(2).getCdnUrl()).into(data.mImg3);
                                        }


                                    } else if (data.getmSocialPostContentsModelsList().size() == 6) {
                                        v6.setVisibility(View.VISIBLE);
                                        v3.setVisibility(View.GONE);
                                        v5.setVisibility(View.GONE);
                                        data.mImg1 = (ImageView) iview.findViewById(R.id.layout6images).findViewById(R.id.imageview_newdesin_1);
                                        data.mImg2 = (ImageView) iview.findViewById(R.id.layout6images).findViewById(R.id.imageview_newdesin_2);
                                        data.mImg3 = (ImageView) iview.findViewById(R.id.layout6images).findViewById(R.id.imageview_newdesin_3);
                                        data.mImg4 = (ImageView) iview.findViewById(R.id.layout6images).findViewById(R.id.imageview_newdesin_4);
                                        data.mImg5 = (ImageView) iview.findViewById(R.id.layout6images).findViewById(R.id.imageview_newdesin_5);
                                        data.mImg6 = (ImageView) iview.findViewById(R.id.layout6images).findViewById(R.id.imageview_newdesin_6);

                                       /*
                                        Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).into( data.mImg1);
                                        Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(1).getCdnUrl()).into( data.mImg2);
                                        Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(2).getCdnUrl()).into( data.mImg3);
                                        Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(3).getCdnUrl()).into( data.mImg4);
                                        Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(4).getCdnUrl()).into( data.mImg5);
                                        Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(5).getCdnUrl()).into( data.mImg6);
                                        */

                                        setclick(data.mImg1, 0, data);
                                        setclick(data.mImg2, 1, data);
                                        setclick(data.mImg3, 2, data);
                                        setclick(data.mImg4, 3, data);
                                        setclick(data.mImg5, 4, data);
                                        setclick(data.mImg6, 5, data);

                                        {
                                            data.mImg1.setVisibility(View.VISIBLE);
                                            ;
                                            //  Picasso.get().load(data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).into(data.mImg1);

                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).into(data.mImg1);
                                        }

                                        {
                                            data.mImg2.setVisibility(View.VISIBLE);

                                            // Picasso.get().load(data.getmSocialPostContentsModelsList().get(1).getCdnUrl()).into(data.mImg2);

                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(1).getCdnUrl()).into(data.mImg2);
                                        }

                                        {
                                            data.mImg3.setVisibility(View.VISIBLE);

                                            //   Picasso.get().load(data.getmSocialPostContentsModelsList().get(2).getCdnUrl()).into(data.mImg3);
                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(2).getCdnUrl()).into(data.mImg3);
                                        }

                                        {
                                            data.mImg4.setVisibility(View.VISIBLE);

                                            // Picasso.get().load(data.getmSocialPostContentsModelsList().get(3).getCdnUrl()).into(data.mImg4);
                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(3).getCdnUrl()).into(data.mImg4);
                                        }

                                        {
                                            data.mImg5.setVisibility(View.VISIBLE);

                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(4).getCdnUrl()).into(data.mImg5);
                                            //  Picasso.get().load(data.getmSocialPostContentsModelsList().get(4).getCdnUrl()).into(data.mImg5);

                                        }
                                        {
                                            data.mImg6.setVisibility(View.VISIBLE);

                                            //   Picasso.get().load(data.getmSocialPostContentsModelsList().get(5).getCdnUrl()).into(data.mImg6);

                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(5).getCdnUrl()).into(data.mImg6);
                                        }
                                    } else if (data.getmSocialPostContentsModelsList().size() == 5) {
                                        v5.setVisibility(View.VISIBLE);
                                        v3.setVisibility(View.GONE);
                                        Log.i("iam", "five in size");
                                        v6.setVisibility(View.GONE);


                                        data.mImg1 = (ImageView) iview.findViewById(R.id.layout5images).findViewById(R.id.imageview_newdesin_1);
                                        data.mImg2 = (ImageView) iview.findViewById(R.id.layout5images).findViewById(R.id.imageview_newdesin_2);
                                        data.mImg3 = (ImageView) iview.findViewById(R.id.layout5images).findViewById(R.id.imageview_newdesin_3);
                                        data.mImg4 = (ImageView) iview.findViewById(R.id.layout5images).findViewById(R.id.imageview_newdesin_4);
                                        data.mImg5 = (ImageView) iview.findViewById(R.id.layout5images).findViewById(R.id.imageview_newdesin_5);

                                        setclick(data.mImg1, 0, data);
                                        setclick(data.mImg2, 1, data);
                                        setclick(data.mImg3, 2, data);
                                        setclick(data.mImg4, 3, data);
                                        setclick(data.mImg5, 4, data);
                                        {
                                            data.mImg1.setVisibility(View.VISIBLE);


                                            //  Picasso.get().load(data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).into(data.mImg1);

                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).into(data.mImg1);
                                        }

                                        {
                                            data.mImg2.setVisibility(View.VISIBLE);

                                            //  Picasso.get().load(data.getmSocialPostContentsModelsList().get(1).getCdnUrl()).into(data.mImg2);

                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(1).getCdnUrl()).into(data.mImg2);
                                        }

                                        {
                                            data.mImg3.setVisibility(View.VISIBLE);

                                            //Picasso.get().load(data.getmSocialPostContentsModelsList().get(2).getCdnUrl()).into(data.mImg3);
                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(2).getCdnUrl()).into(data.mImg3);
                                        }

                                        {
                                            data.mImg4.setVisibility(View.VISIBLE);

                                            //Picasso.get().load(data.getmSocialPostContentsModelsList().get(3).getCdnUrl()).into(data.mImg4);
                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(3).getCdnUrl()).into(data.mImg4);
                                        }

                                        {
                                            data.mImg5.setVisibility(View.VISIBLE);

                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(4).getCdnUrl()).into(data.mImg5);
                                        }

                                    }
                                    grid.setVisibility(View.GONE);
                                } else if (1 == 2) {

                                    adapter = new SocialPostsGridContentsAdapter(getContext(), data.getmSocialPostContentsModelsList(), data.getPostid(), getSocialHomeActivity(), grid, data.getmSocialPostsModel().getUserID());
                                    grid.setAdapter(adapter);
                                    v3.setVisibility(View.GONE);
                                    v5.setVisibility(View.GONE);
                                    v6.setVisibility(View.GONE);
                                    grid.setVisibility(View.VISIBLE);
                                }


                            } else {
                                millavideoplayer.setVisibility(View.VISIBLE);
                                v3.setVisibility(View.GONE);
                                v5.setVisibility(View.GONE);
                                v6.setVisibility(View.GONE);
                                grid.setVisibility(View.GONE);
                                if (data.getmSocialPostContentsModelsList().get(0).getContentType().equals("1")) {
                                    //  millavideoplayer.setVisibility(View.GONE)


                                    if (data.getmSocialPostContentsModelsList().get(0).getCdnUrl() != null) {
                                        RequestOptions options = new RequestOptions().placeholder(R.drawable.videoimageloader).centerCrop();
                                        Glide.with(getContext()).load(
                                                data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).apply(options)
                                                .into(data.custom_video_viewImageview);
                                        Log.i("getCdnUrl", data.getmSocialPostContentsModelsList().get(0).getCdnUrl());
                                    } else {
                                        Log.i("getCdnUrl", "is null");
                                    }

                                    ///millavideoplayer.setVisibility(View.GONE);
                                } else if (data.getmSocialPostContentsModelsList().get(0).getContentType().equals("2")) {
                                    RequestOptions options = new RequestOptions().placeholder(R.drawable.videoimageloader).centerCrop();


                                    sethight3(data.getmSocialPostContentsModelsList(), data.custom_video_viewImageview, millavideoplayer);

                                    viewviewoverlay.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            Intent intent = new Intent(getActivity(), SocialCommentActivityNew.class);
                                            //  pausePreviousVideos();
                                            intent.putExtra("post_id", data.getPostid());
                                            startActivity(intent);
                                            if (1 == 2) {
                                                if (data.getmSocialPostsModel().isSharedPost()) {
                                                    frag[0] = new SocialCommnetsActivity();
                                                    b[0] = new Bundle();
                                                    b[0].putString("post_id", data.getmSocialPostsModel().getSharedPostID());
                                                    frag[0].setArguments(b[0]);
                                                    getSocialHomeActivity().fragTransaction(frag[0]);
                                                } else {
                                                    frag[0] = new SocialCommnetsActivity();
                                                    b[0] = new Bundle();
                                                    b[0].putString("post_id", data.getPostid());
                                                    frag[0].setArguments(b[0]);
                                                    getSocialHomeActivity().fragTransaction(frag[0]);
                                                }
                                            }

                                        }
                                    });


                                    //  data.buffer_video.setVisibility(View.VISIBLE);
                                    // data.buffer_video.setText("loading ...914");
                                    // millavideoplayer.setVisibility(View.VISIBLE);
                                    int pos = l.findLastVisibleItemPosition();
                                    Log.i("video", new Gson().toJson(data.getmSocialPostContentsModelsList().get(0)));
                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).apply(options).into(data.custom_video_viewImageview);
                                    //setDefaults(data.getmSocialPostsModel().getmSocialPostsid(),data.getmSocialPostContentsModelsList().get(0).getFileUrl(),getContext());

                                } else {

                                    millavideoplayer.setVisibility(View.GONE);
                                    grid.setVisibility(View.GONE);
                                    v3.setVisibility(View.GONE);
                                    v5.setVisibility(View.GONE);
                                    v6.setVisibility(View.GONE);

                                }
                            }

                        } else {
                            Log.i("map", "ignored");
                            // millavideoplayer.setVisibility(View.GONE);
                            millavideoplayer.setVisibility(View.VISIBLE);
                            grid.setVisibility(View.GONE);
                            v3.setVisibility(View.GONE);
                            v5.setVisibility(View.GONE);
                            v6.setVisibility(View.GONE);

                            try {
                                if (!data.getmSocialPostsModel().getLatitude().equals("") && !data.getmSocialPostsModel().getLongitude().equals("")) {

                                    Log.i("map", "found");
                                    RequestOptions options = new RequestOptions().centerCrop().placeholder(R.drawable.videoimageloader);
                                    Glide.with(context)
                                            .load("https://maps.googleapis.com/maps/api/staticmap?center=" + data.getmSocialPostsModel().getLocation() + "&zoom=11&size=1024x768&maptype=roadmap&markers=color:red|" + data.getmSocialPostsModel().getLatitude() + "," + data.getmSocialPostsModel().getLongitude() + "&key=" + GOOGLELOCATIONAPIKEY)
                                            .apply(options)
                                            .into(data.custom_video_viewImageview);
                                } else {
                                    Log.i("map", "!found");

                                }
                            } catch (Exception e) {
                                Log.e("map", e.getMessage() + "");


                            }

                            // TODO: 18/1/19 need to check why data is missing
                            Log.i("content", "ismissing");
                        }

                        if (data.getI() == slimedata.size() - 1) {
                            getSocialUserPostsMilla();
                        } else {
                            Log.i("cdata", data.getI() + "  " + slimedata.size());
                        }

                    }
                })
                .register(R.layout.adapter_social_posts_img5, new SlimInjector<MultiImages5>() {
                    @Override
                    public void onInject(MultiImages5 data5, IViewInjector iview) {

                                    MainDetails data = data5.getMainDetails();
                                    List<SocialPostCountersModel> postCountersModelList = new ArrayList<>();
                                    final Bundle[] b = new Bundle[1];
                                    final Fragment[] frag = {null};

                                    ImageView viewviewoverlay;
                                    FrameLayout millavideoplayer;
                                    Context context = getContext();
                                    data.setMultiple(false);
                                    ClickAction actionClick;
                                    RelativeLayout emojiesDialog;
                                    ReactionListAdapter reactionListAdapter;
                                    TextView txt_time, txt_like_count, txt_comment, txt_share, txt_title, txt_content, reactionCount, commentCount;
                                    ImageView ProfileImage, reactionIcon;
                                    RecyclerView icons;
                                    RecyclerView grid;
                                    View v5;
                                    ImageView img1, img2, img3, img4, img5, img6;
                                    LinearLayout commLayout, more_menu_layout, click_like, like_comment_Lyt;


                                    final ReactionView[] rvl = {null};//new ReactionView();

                                    SocialPostsGridContentsAdapter adapter;
//                        LinearLayout like, angry, cry, happy, wow, love,
                                    LinearLayout pastMainLayout, shareLayout;

                                    // data.buffer_video = (TextView) view.findViewById(R.id.buffer_video);
                                    // data.buffer_video.setTextColor(getActivity().getResources().getColor(R.color.red));


                                    v5 = (View) iview.findViewById(R.id.layout5images);
                                    v5.setVisibility(View.VISIBLE);


                                    commLayout = (LinearLayout) iview.findViewById(R.id.social_layout_comments);
                                    commLayout.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent intent = new Intent(getActivity(), SocialCommentActivityNew.class);
                                            //  pausePreviousVideos();
                                            intent.putExtra("post_id", data.getPostid());
                                            startActivity(intent);
                                        }
                                    });

                                    click_like = (LinearLayout) iview.findViewById(R.id.click_like);
                                    click_like.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            onClickLike(data.getPostid(),"like");
                                        }
                                    });
                                    txt_time = (TextView) iview.findViewById(R.id.txt_date);
                                    commentCount = (TextView) iview.findViewById(R.id.comment_count);
                                    txt_like_count = (TextView) iview.findViewById(R.id.txt_like_count);
                                    txt_comment = (TextView) iview.findViewById(R.id.txt_comment);
                                    txt_share = (TextView) iview.findViewById(R.id.txt_share);
                                    txt_title = (TextView) iview.findViewById(R.id.txt_title);
                                    ProfileImage = (ImageView) iview.findViewById(R.id.imgUser);
                                    txt_content = (TextView) iview.findViewById(R.id.txt_content);

                                    grid = (RecyclerView) iview.findViewById(R.id.gridview);
                                    more_menu_layout = (LinearLayout) iview.findViewById(R.id.more_menu_layout);

                                    like_comment_Lyt = (LinearLayout) iview.findViewById(R.id.comment_reaction_layout);
                                    reactionCount = (TextView) iview.findViewById(R.id.reaction_count);
                                    reactionIcon = (ImageView) iview.findViewById(R.id.reaction_icon);
                                    icons = (RecyclerView) iview.findViewById(R.id.reaction_list_recycler);

                                    emojiesDialog = (RelativeLayout) iview.findViewById(R.id.emojies_layout);
//                        like = (LinearLayout) iview.findViewById(R.id.lyt_like_button);
//                        angry = (LinearLayout) iview.findViewById(R.id.lyt_angry_button);
//                        cry = (LinearLayout) iview.findViewById(R.id.lyt_cry_button);
//                        happy = (LinearLayout) iview.findViewById(R.id.lyt_happy_button);
//                        wow = (LinearLayout) iview.findViewById(R.id.lyt_wow_button);
//                        love = (LinearLayout) iview.findViewById(R.id.lyt_love_button);
                                    pastMainLayout = (LinearLayout) iview.findViewById(R.id.past_main_layout);
                                    shareLayout = (LinearLayout) iview.findViewById(R.id.click_share);


                                        shareLayout.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                ArrayList<SocialPostContentsModel.DataBean> array = new ArrayList<>();
                                                Log.i("postidfinding_0",data.getPostid()+" "+data.getmSocialUserPostsModel().getmSocialPostsid());
                                                for(int i =0;i<data.getmSocialPostContentsModelsList().size();i++) {
                                                    SocialPostContentsModel.DataBean datamodel = new SocialPostContentsModel.DataBean();

                                                    try{
                                                        datamodel.setCdnUrl(data.getmSocialPostContentsModelsList().get(i).getCdnUrl());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setContentType(data.getmSocialPostContentsModelsList().get(i).getContentType());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setPostId(data.getmSocialPostContentsModelsList().get(i).getPostId());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setFileName(data.getmSocialPostContentsModelsList().get(i).getFileName());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setFileType(data.getmSocialPostContentsModelsList().get(i).getFileType());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setFileUrl(data.getmSocialPostContentsModelsList().get(i).getFileUrl());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setPostId(data.getPostid());
                                                    }catch (Exception e){}
                                                    Log.i("postidfinding"+i,new   Gson().toJson(data.getmSocialPostContentsModelsList())+"");
                                                    try{
                                                        array.add(datamodel);
                                                    }catch (Exception e){}
                                                }
                                                createShareMenu(array).show();
                                            }

                                        });
                                    int position = data.getI();


                                    if (data.getmSocialUserDetailsModel() != null && (data.getmSocialUserDetailsModel().getUser_name() != null) ? true : false) {
                                        txt_title.setText(data.getmSocialUserDetailsModel().getUser_name() + "");
                                        RequestOptions options = new RequestOptions().centerCrop();
                                        Glide.with(getContext()).load(data.getmSocialUserDetailsModel().getImage()).apply(options)
                                                .into(ProfileImage);
                                    }

                                    if (data.getmSocialPostContentsModelsList() != null ?
                                            (data.getmSocialPostContentsModelsList().size() > 0 ? true : false) : false) {
                                        //millavideoplayer.setVisibility(View.GONE);

                                        if (data.getmSocialPostContentsModelsList().size() > 1) {


                                            // millavideoplayer.setVisibility(View.GONE);
                                            data.setMultiple(true);


                                            if (data.getmSocialPostContentsModelsList().size() == 5) {


                                                data.mImg1 = (ImageView) iview.findViewById(R.id.layout5images).findViewById(R.id.imageview_newdesin_1);
                                                data.mImg2 = (ImageView) iview.findViewById(R.id.layout5images).findViewById(R.id.imageview_newdesin_2);
                                                data.mImg3 = (ImageView) iview.findViewById(R.id.layout5images).findViewById(R.id.imageview_newdesin_3);
                                                data.mImg4 = (ImageView) iview.findViewById(R.id.layout5images).findViewById(R.id.imageview_newdesin_4);
                                                data.mImg5 = (ImageView) iview.findViewById(R.id.layout5images).findViewById(R.id.imageview_newdesin_5);

                                                setclick(data.mImg1, 0, data);
                                                setclick(data.mImg2, 1, data);
                                                setclick(data.mImg3, 2, data);
                                                setclick(data.mImg4, 3, data);
                                                setclick(data.mImg5, 4, data);
                                                {
                                                    data.mImg1.setVisibility(View.VISIBLE);

                                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).into(data.mImg1);
                                                }

                                                {
                                                    data.mImg2.setVisibility(View.VISIBLE);

                                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(1).getCdnUrl()).into(data.mImg2);
                                                }

                                                {
                                                    data.mImg3.setVisibility(View.VISIBLE);

                                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(2).getCdnUrl()).into(data.mImg3);
                                                }

                                                {
                                                    data.mImg4.setVisibility(View.VISIBLE);

                                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(3).getCdnUrl()).into(data.mImg4);
                                                }

                                                {
                                                    data.mImg5.setVisibility(View.VISIBLE);

                                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(4).getCdnUrl()).into(data.mImg5);
                                                }
                                            }
                                            grid.setVisibility(View.GONE);
                                        }
                                        grid.setVisibility(View.VISIBLE);


                                    }

                                    if (data.getI() == slimedata.size() - 1) {
                                        getSocialUserPostsMilla();
                                    } else {
                                        Log.i("cdata", data.getI() + "  " + slimedata.size());
                                    }


                    }
                })
                .register(R.layout.adapter_social_posts_img3, new SlimInjector<MultiImages3>() {
                    @Override
                    public void onInject(MultiImages3 data1, IViewInjector iview) {

                                    MainDetails data = data1.getMainDetails();
                                    List<SocialPostCountersModel> postCountersModelList = new ArrayList<>();
                                    final Bundle[] b = new Bundle[1];
                                    final Fragment[] frag = {null};

                                    ImageView viewviewoverlay;
                                    FrameLayout millavideoplayer;
                                    Context context = getContext();
                                    data.setMultiple(false);
                                    ClickAction actionClick;
                                    RelativeLayout emojiesDialog;
                                    ReactionListAdapter reactionListAdapter;
                                    TextView txt_time, txt_like_count, txt_comment, txt_share, txt_title, txt_content, reactionCount, commentCount;
                                    ImageView ProfileImage, reactionIcon;
                                    RecyclerView icons;

                                    View v3;
                                    ImageView img1, img2, img3, img4, img5, img6;
                                    LinearLayout commLayout, more_menu_layout, click_like, like_comment_Lyt;

                                    TextView txt_location_view;
                                    final ReactionView[] rvl = {null};//new ReactionView();

                                    SocialPostsGridContentsAdapter adapter;
//                        LinearLayout like, angry, cry, happy, wow, love,
                                    LinearLayout pastMainLayout, shareLayout, layout_location_view;

                                    // data.buffer_video = (TextView) view.findViewById(R.id.buffer_video);
                                    // data.buffer_video.setTextColor(getActivity().getResources().getColor(R.color.red));
                                    v3 = (View) iview.findViewById(R.id.layout3images);
                                    v3.setVisibility(View.VISIBLE);


                                    layout_location_view = (LinearLayout) iview.findViewById(R.id.layout_location_view);
                                    txt_location_view = (TextView) iview.findViewById(R.id.txt_location_view);
                                    commLayout = (LinearLayout) iview.findViewById(R.id.social_layout_comments);



                                    commLayout.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent intent = new Intent(getActivity(), SocialCommentActivityNew.class);
                                            //  pausePreviousVideos();
                                            intent.putExtra("post_id", data.getPostid());
                                            startActivity(intent);
                                        }
                                    });
                                    click_like = (LinearLayout) iview.findViewById(R.id.click_like);
                                    click_like.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            onClickLike(data.getPostid(),"like");
                                        }
                                    });
                                    txt_time = (TextView) iview.findViewById(R.id.txt_date);
                                    commentCount = (TextView) iview.findViewById(R.id.comment_count);
                                    txt_like_count = (TextView) iview.findViewById(R.id.txt_like_count);
                                    txt_comment = (TextView) iview.findViewById(R.id.txt_comment);
                                    txt_share = (TextView) iview.findViewById(R.id.txt_share);
                                    txt_title = (TextView) iview.findViewById(R.id.txt_title);
                                    ProfileImage = (ImageView) iview.findViewById(R.id.imgUser);
                                    txt_content = (TextView) iview.findViewById(R.id.txt_content);


                                    more_menu_layout = (LinearLayout) iview.findViewById(R.id.more_menu_layout);

                                    like_comment_Lyt = (LinearLayout) iview.findViewById(R.id.comment_reaction_layout);
                                    reactionCount = (TextView) iview.findViewById(R.id.reaction_count);
                                    reactionIcon = (ImageView) iview.findViewById(R.id.reaction_icon);
                                    icons = (RecyclerView) iview.findViewById(R.id.reaction_list_recycler);

                                    emojiesDialog = (RelativeLayout) iview.findViewById(R.id.emojies_layout);
//                        like = (LinearLayout) iview.findViewById(R.id.lyt_like_button);
//                        angry = (LinearLayout) iview.findViewById(R.id.lyt_angry_button);
//                        cry = (LinearLayout) iview.findViewById(R.id.lyt_cry_button);
//                        happy = (LinearLayout) iview.findViewById(R.id.lyt_happy_button);
//                        wow = (LinearLayout) iview.findViewById(R.id.lyt_wow_button);
//                        love = (LinearLayout) iview.findViewById(R.id.lyt_love_button);
                                    pastMainLayout = (LinearLayout) iview.findViewById(R.id.past_main_layout);
                                    shareLayout = (LinearLayout) iview.findViewById(R.id.click_share);

                                        shareLayout.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                ArrayList<SocialPostContentsModel.DataBean> array = new ArrayList<>();
                                                Log.i("postidfinding_0",data.getPostid()+" "+data.getmSocialUserPostsModel().getmSocialPostsid());
                                                for(int i =0;i<data.getmSocialPostContentsModelsList().size();i++) {
                                                    SocialPostContentsModel.DataBean datamodel = new SocialPostContentsModel.DataBean();

                                                    try{
                                                        datamodel.setCdnUrl(data.getmSocialPostContentsModelsList().get(i).getCdnUrl());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setContentType(data.getmSocialPostContentsModelsList().get(i).getContentType());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setPostId(data.getmSocialPostContentsModelsList().get(i).getPostId());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setFileName(data.getmSocialPostContentsModelsList().get(i).getFileName());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setFileType(data.getmSocialPostContentsModelsList().get(i).getFileType());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setFileUrl(data.getmSocialPostContentsModelsList().get(i).getFileUrl());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setPostId(data.getPostid());
                                                    }catch (Exception e){}
                                                    Log.i("postidfinding"+i,new   Gson().toJson(data.getmSocialPostContentsModelsList())+"");
                                                    try{
                                                        array.add(datamodel);
                                                    }catch (Exception e){}
                                                }
                                                createShareMenu(array).show();
                                            }
                                        });

                                    int position = data.getI();


                                    if (data.getmSocialUserDetailsModel() != null && (data.getmSocialUserDetailsModel().getUser_name() != null) ? true : false) {
                                        txt_title.setText(data.getmSocialUserDetailsModel().getUser_name() + "");
                                        RequestOptions options = new RequestOptions().centerCrop();
                                        Glide.with(getContext()).load(data.getmSocialUserDetailsModel().getImage()).apply(options)
                                                .into(ProfileImage);
                                    }

                                    if (data.getmSocialPostContentsModelsList() != null ?
                                            (data.getmSocialPostContentsModelsList().size() > 0 ? true : false) : false) {
                                        //millavideoplayer.setVisibility(View.GONE);

                                        if (data.getmSocialPostContentsModelsList().size() > 1) {


                                            // millavideoplayer.setVisibility(View.GONE);
                                            data.setMultiple(true);

                                            // TODO: 23/1/19 check here


                                            if (data.getmSocialPostContentsModelsList().size() == 3) {
                                                Log.i("image3", new Gson().toJson(data.getmSocialPostContentsModelsList().get(0)));


                                                data.mImg1 = (ImageView) iview.findViewById(R.id.layout3images).findViewById(R.id.imageview_newdesin_1);
                                                data.mImg2 = (ImageView) iview.findViewById(R.id.layout3images).findViewById(R.id.imageview_newdesin_2);
                                                data.mImg3 = (ImageView) iview.findViewById(R.id.layout3images).findViewById(R.id.imageview_newdesin_3);
                                                setclick(data.mImg1, 0, data);
                                                setclick(data.mImg2, 1, data);
                                                setclick(data.mImg3, 2, data);
                                                data.mImg1 = (ImageView) iview.findViewById(R.id.layout3images).findViewById(R.id.imageview_newdesin_1);
                                                data.mImg2 = (ImageView) iview.findViewById(R.id.layout3images).findViewById(R.id.imageview_newdesin_2);
                                                data.mImg3 = (ImageView) iview.findViewById(R.id.layout3images).findViewById(R.id.imageview_newdesin_3);
                                                setclick(data.mImg1, 0, data);
                                                setclick(data.mImg2, 1, data);
                                                setclick(data.mImg3, 2, data);
                                                {
                                                    data.mImg1.setVisibility(View.VISIBLE);

                                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).into(data.mImg1);
                                                }

                                                {
                                                    data.mImg2.setVisibility(View.VISIBLE);

                                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(1).getCdnUrl()).into(data.mImg2);
                                                }

                                                {
                                                    data.mImg3.setVisibility(View.VISIBLE);

                                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(2).getCdnUrl()).into(data.mImg3);
                                                }


                                            }

                                        }


                                    } else {

                                    }


                                    if (data.getI() == slimedata.size() - 1) {
                                        getSocialUserPostsMilla();
                                    } else {
                                        Log.i("cdata", data.getI() + "  " + slimedata.size());
                                    }

                    }
                })
                .register(R.layout.below_toolbar_header_layout, new SlimInjector<String>() {
                    @Override
                    public void onInject(String data, IViewInjector view) {

                        LinearLayout location_post_button, image_post_button, video_post_button, live_post_button;
                        video_post_button = (LinearLayout) view.findViewById(R.id.video_post_button);
                        video_post_button.setOnClickListener(v -> {
                            Log.i("video_post_button", "clicked");
                            TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(getSocialHomeActivity())
                                    .setOnMultiImageSelectedListener(uriList -> {

                                        for (int i = 0; i < uriList.size(); i++) {
                                            SocialPostContentsModel.DataBean model = new SocialPostContentsModel.DataBean();
                                            String name = Utils.getRealPathFromURI(getSocialHomeActivity(), uriList.get(i));
                                            model.setCdnUrl(name);
                                            model.setContentType("2");
                                            model.setFileType("VID");
                                            list.add(model);
                                        }
                                        if (!list.isEmpty()) {

                                            SocialAddPostFragment home = new SocialAddPostFragment();
                                            Bundle bundle = new Bundle();
                                            bundle.putSerializable("datas", list);
                                            home.setArguments(bundle);
                                            getSocialHomeActivity().addFragment(home);

                                        }
                                    })
                                    .showVideoMedia()
                                    .showTitle(false)
                                    .setSelectMaxCount(1)
                                    .setCompleteButtonText(getString(R.string.done))
                                    .setEmptySelectionText(getString(R.string.no_video_select))
                                    .create();

                            bottomSheetDialogFragment.show(getFragmentManager());

                        });

                        location_post_button = (LinearLayout) view.findViewById(R.id.location_post_button);
                        location_post_button.setOnClickListener(v -> getLocation());
                        location_post_button.setOnClickListener(v -> getLocations());

                        image_post_button = (LinearLayout) view.findViewById(R.id.image_post_button);
                        image_post_button.setOnClickListener(v -> {
                            Log.i("image_post_button", "clicked");
                            TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(getSocialHomeActivity())
                                    .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                                        @Override
                                        public void onImagesSelected(ArrayList<Uri> uriList) {


                                            for (int i = 0; i < uriList.size(); i++) {
                                                SocialPostContentsModel.DataBean model = new SocialPostContentsModel.DataBean();
                                                String name = Utils.getRealPathFromURI(getSocialHomeActivity(), uriList.get(i));
                                                Log.i("result_name_" + i, name);
                                                model.setCdnUrl(name);
                                                model.setContentType("1");
                                                model.setFileType("IMG");
                                                list.add(model);
                                            }
                                            if (!list.isEmpty()) {

                                                SocialAddPostFragment home = new SocialAddPostFragment();
                                                Bundle bundle = new Bundle();
                                                bundle.putSerializable("datas", list);
                                                home.setArguments(bundle);
                                                getSocialHomeActivity().addFragment(home);
                                                //list.clear();
//                                getFragmentManager().beginTransaction().replace(R.id.frame, home).addToBackStack(null).commit();
                                            }
                                        }
                                    })
                                    .showTitle(false)
                                    .setSelectMaxCount(6)
                                    .setCompleteButtonText(getString(R.string.done))
                                    .setEmptySelectionText(getString(R.string.no_images_select))
                                    .create();

                            bottomSheetDialogFragment.show(getFragmentManager());
                        });


                        /*video_post_button.setOnClickListener(v -> {
                            TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(getSocialHomeActivity())
                                    .setOnMultiImageSelectedListener(uriList -> {
                                        for (int i = 0; i < uriList.size(); i++) {
                                            SocialPostContentsModel.DataBean model = new SocialPostContentsModel.DataBean();
                                            String name = Utils.getRealPathFromURI(getSocialHomeActivity(), uriList.get(i));
                                            model.setCdnUrl(name);
                                            model.setContentType("2");
                                            model.setFileType("VID");
                                            list.add(model);
                                        }
                                        if (!list.isEmpty()) {
                                            SocialAddPostFragment home = new SocialAddPostFragment();
                                            Bundle bundle = new Bundle();
                                            bundle.putSerializable("datas", list);
                                            home.setArguments(bundle);
                                            getSocialHomeActivity().addFragment(home);
//                            getSocialHomeActivity().addFragment(home);
//                            getFragmentManager().beginTransaction().replace(R.id.frame, home).addToBackStack(null).commit();
                                        }
                                    })
                                    .showVideoMedia()
                                    .showTitle(false)
                                    .setSelectMaxCount(6)
                                    .setCompleteButtonText(getString(R.string.done))
                                    .setEmptySelectionText(getString(R.string.no_video_select))
                                    .create();

                            bottomSheetDialogFragment.show(getFragmentManager());

                        });*/
                        addPost = (TextView) view.findViewById(R.id.txt_write_something);
                        addPost.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SocialAddPostFragment home = new SocialAddPostFragment();
                                getSocialHomeActivity().addFragment(home);
//                getFragmentManager().beginTransaction().replace(R.id.frame, home).addToBackStack(null).commit();

                            }
                        });

                        layout_search.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SocialFriendsFragment home = new SocialFriendsFragment();
                                getSocialHomeActivity().addFragment(home);
                            }
                        });

                        live_post_button = (LinearLayout) view.findViewById(R.id.live_post_button);
                        live_post_button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //GoLive();

                                Calendar d = Calendar.getInstance();
                                Long Channel_id = (d.getTime()).getTime();

                                Intent i = new Intent(getContext(), LiveRoomActivity.class);
                                i.putExtra(ConstantApp.ACTION_KEY_CROLE, io.agora.rtc.Constants.CLIENT_ROLE_BROADCASTER);
                                i.putExtra(ConstantApp.ACTION_KEY_ROOM_NAME, Channel_id + "");
                                i.putExtra("Social", "1");
//                i.putExtra(ConstantApp.ACTION_KEY_ROOM_NAME, modelList.get(position).getProperty_id());
                                startActivity(i);

                            }
                        });


                        CircleImageView profilepick = (CircleImageView) view.findViewById(R.id.prof_pic);

                        if (preference.getSocialImage() != null) {
                            Glide.with(getContext()).load(preference.getSocialImage()).into(profilepick);
                        }


                        RequestOptions options = new RequestOptions().centerCrop();
                        Glide.with(Objects.requireNonNull(getContext())).load(preference.getSocialImage()).apply(options).into(prof_pic);

                    }
                })
                .register(R.layout.adapter_social_posts_img6, new SlimInjector<MultiImages6>() {
                    @Override
                    public void onInject(MultiImages6 data6, IViewInjector iview) {

                                    MainDetails data = data6.getMainDetails();
                                    List<SocialPostCountersModel> postCountersModelList = new ArrayList<>();
                                    final Bundle[] b = new Bundle[1];
                                    final Fragment[] frag = {null};

                                    ImageView viewviewoverlay;
                                    FrameLayout millavideoplayer;
                                    Context context = getContext();
                                    data.setMultiple(false);
                                    ClickAction actionClick;
                                    RelativeLayout emojiesDialog;
                                    ReactionListAdapter reactionListAdapter;
                                    TextView txt_time, txt_like_count, txt_comment, txt_share, txt_title, txt_content, reactionCount, commentCount;
                                    ImageView ProfileImage, reactionIcon;
                                    RecyclerView icons;
                                    RecyclerView grid;
                                    View v6;
                                    ImageView img1, img2, img3, img4, img5, img6;
                                    LinearLayout commLayout, more_menu_layout, click_like, like_comment_Lyt;

                                    TextView txt_location_view;
                                    final ReactionView[] rvl = {null};//new ReactionView();

                                    SocialPostsGridContentsAdapter adapter;
//                        LinearLayout like, angry, cry, happy, wow, love,
                                    LinearLayout pastMainLayout, shareLayout, layout_location_view;

                                    // data.buffer_video = (TextView) view.findViewById(R.id.buffer_video);
                                    // data.buffer_video.setTextColor(getActivity().getResources().getColor(R.color.red));


                                    v6 = (View) iview.findViewById(R.id.layout6images);
                                    v6.setVisibility(View.VISIBLE);


                                    //  millavideoplayer.setVisibility(View.GONE);
                                    viewviewoverlay = (ImageView) iview.findViewById(R.id.viewviewoverlay);

                                    layout_location_view = (LinearLayout) iview.findViewById(R.id.layout_location_view);
                                    txt_location_view = (TextView) iview.findViewById(R.id.txt_location_view);
                                    commLayout = (LinearLayout) iview.findViewById(R.id.social_layout_comments);
                                    commLayout.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent intent = new Intent(getActivity(), SocialCommentActivityNew.class);
                                            //  pausePreviousVideos();
                                            intent.putExtra("post_id", data.getPostid());
                                            startActivity(intent);
                                        }
                                    });
                                    click_like = (LinearLayout) iview.findViewById(R.id.click_like);
                                    click_like.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            onClickLike(data.getPostid(),"like");
                                        }
                                    });
                                    txt_time = (TextView) iview.findViewById(R.id.txt_date);
                                    commentCount = (TextView) iview.findViewById(R.id.comment_count);
                                    txt_like_count = (TextView) iview.findViewById(R.id.txt_like_count);
                                    txt_comment = (TextView) iview.findViewById(R.id.txt_comment);
                                    txt_share = (TextView) iview.findViewById(R.id.txt_share);
                                    txt_title = (TextView) iview.findViewById(R.id.txt_title);
                                    ProfileImage = (ImageView) iview.findViewById(R.id.imgUser);
                                    txt_content = (TextView) iview.findViewById(R.id.txt_content);


                                    more_menu_layout = (LinearLayout) iview.findViewById(R.id.more_menu_layout);

                                    like_comment_Lyt = (LinearLayout) iview.findViewById(R.id.comment_reaction_layout);
                                    reactionCount = (TextView) iview.findViewById(R.id.reaction_count);
                                    reactionIcon = (ImageView) iview.findViewById(R.id.reaction_icon);
                                    icons = (RecyclerView) iview.findViewById(R.id.reaction_list_recycler);

                                    emojiesDialog = (RelativeLayout) iview.findViewById(R.id.emojies_layout);
//                        like = (LinearLayout) iview.findViewById(R.id.lyt_like_button);
//                        angry = (LinearLayout) iview.findViewById(R.id.lyt_angry_button);
//                        cry = (LinearLayout) iview.findViewById(R.id.lyt_cry_button);
//                        happy = (LinearLayout) iview.findViewById(R.id.lyt_happy_button);
//                        wow = (LinearLayout) iview.findViewById(R.id.lyt_wow_button);
//                        love = (LinearLayout) iview.findViewById(R.id.lyt_love_button);
                                    pastMainLayout = (LinearLayout) iview.findViewById(R.id.past_main_layout);
                                    shareLayout = (LinearLayout) iview.findViewById(R.id.click_share);


                                        shareLayout.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                ArrayList<SocialPostContentsModel.DataBean> array = new ArrayList<>();
                                                Log.i("postidfinding_0",data.getPostid()+" "+data.getmSocialUserPostsModel().getmSocialPostsid());
                                                for(int i =0;i<data.getmSocialPostContentsModelsList().size();i++) {
                                                    SocialPostContentsModel.DataBean datamodel = new SocialPostContentsModel.DataBean();

                                                    try{
                                                        datamodel.setCdnUrl(data.getmSocialPostContentsModelsList().get(i).getCdnUrl());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setContentType(data.getmSocialPostContentsModelsList().get(i).getContentType());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setPostId(data.getmSocialPostContentsModelsList().get(i).getPostId());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setFileName(data.getmSocialPostContentsModelsList().get(i).getFileName());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setFileType(data.getmSocialPostContentsModelsList().get(i).getFileType());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setFileUrl(data.getmSocialPostContentsModelsList().get(i).getFileUrl());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setPostId(data.getPostid());
                                                    }catch (Exception e){}
                                                    Log.i("postidfinding"+i,new   Gson().toJson(data.getmSocialPostContentsModelsList())+"");
                                                    try{
                                                        array.add(datamodel);
                                                    }catch (Exception e){}
                                                }
                                                createShareMenu(array).show();
                                            }
                                        });

                                    int position = data.getI();


                                    if (data.getmSocialUserDetailsModel() != null && (data.getmSocialUserDetailsModel().getUser_name() != null) ? true : false) {
                                        txt_title.setText(data.getmSocialUserDetailsModel().getUser_name() + "");
                                        RequestOptions options = new RequestOptions().centerCrop();
                                        Glide.with(getContext()).load(data.getmSocialUserDetailsModel().getImage()).apply(options)
                                                .into(ProfileImage);
                                    }

                                    if (data.getmSocialPostContentsModelsList() != null ?
                                            (data.getmSocialPostContentsModelsList().size() > 0 ? true : false) : false) {
                                        //millavideoplayer.setVisibility(View.GONE);

                                        if (data.getmSocialPostContentsModelsList().size() > 1) {


                                            if (data.getmSocialPostContentsModelsList().size() == 6) {
                                                v6.setVisibility(View.VISIBLE);


                                                data.mImg1 = (ImageView) iview.findViewById(R.id.layout6images).findViewById(R.id.imageview_newdesin_1);
                                                data.mImg2 = (ImageView) iview.findViewById(R.id.layout6images).findViewById(R.id.imageview_newdesin_2);
                                                data.mImg3 = (ImageView) iview.findViewById(R.id.layout6images).findViewById(R.id.imageview_newdesin_3);
                                                data.mImg4 = (ImageView) iview.findViewById(R.id.layout6images).findViewById(R.id.imageview_newdesin_4);
                                                data.mImg5 = (ImageView) iview.findViewById(R.id.layout6images).findViewById(R.id.imageview_newdesin_5);
                                                data.mImg6 = (ImageView) iview.findViewById(R.id.layout6images).findViewById(R.id.imageview_newdesin_6);

                                       /*
                                        Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).into( data.mImg1);
                                        Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(1).getCdnUrl()).into( data.mImg2);
                                        Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(2).getCdnUrl()).into( data.mImg3);
                                        Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(3).getCdnUrl()).into( data.mImg4);
                                        Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(4).getCdnUrl()).into( data.mImg5);
                                        Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(5).getCdnUrl()).into( data.mImg6);
                                        */

                                                setclick(data.mImg1, 0, data);
                                                setclick(data.mImg2, 1, data);
                                                setclick(data.mImg3, 2, data);
                                                setclick(data.mImg4, 3, data);
                                                setclick(data.mImg5, 4, data);
                                                setclick(data.mImg6, 5, data);

                                                {
                                                    data.mImg1.setVisibility(View.VISIBLE);

                                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).into(data.mImg1);
                                                }

                                                {
                                                    data.mImg2.setVisibility(View.VISIBLE);

                                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(1).getCdnUrl()).into(data.mImg2);
                                                }

                                                {
                                                    data.mImg3.setVisibility(View.VISIBLE);

                                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(2).getCdnUrl()).into(data.mImg3);
                                                }

                                                {
                                                    data.mImg4.setVisibility(View.VISIBLE);

                                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(3).getCdnUrl()).into(data.mImg4);
                                                }

                                                {
                                                    data.mImg5.setVisibility(View.VISIBLE);

                                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(4).getCdnUrl()).into(data.mImg5);
                                                }
                                                {
                                                    data.mImg6.setVisibility(View.VISIBLE);

                                                    Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(5).getCdnUrl()).into(data.mImg6);
                                                }
                                            }


                                        }

                                    }

                                    if (data.getI() == slimedata.size() - 1) {
                                        getSocialUserPostsMilla();
                                    } else {
                                        Log.i("cdata", data.getI() + "  " + slimedata.size());
                                    }


                    }
                })
                .register(R.layout.adapter_social_posts_video1, new SlimInjector<VideoOnly>() {
                    @Override
                    public void onInject(VideoOnly data1, IViewInjector iview) {

                        Log.i("oppo","video called");
                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    MainDetails data = data1.getMainDetails();
                                    List<SocialPostCountersModel> postCountersModelList = new ArrayList<>();
                                    final Bundle[] b = new Bundle[1];
                                    final Fragment[] frag = {null};

                                    VideoView videoView;
                                    ImageView viewviewoverlay;
                                    FrameLayout millavideoplayer;
                                    Context context = getContext();
                                    data.setMultiple(false);
                                    ClickAction actionClick;
                                    RelativeLayout emojiesDialog;
                                    ReactionListAdapter reactionListAdapter;
                                    TextView txt_time, txt_like_count, txt_comment, txt_share, txt_title, txt_content, reactionCount, commentCount;
                                    ImageView ProfileImage, reactionIcon;
                                    RecyclerView icons;


                                    LinearLayout commLayout, more_menu_layout, click_like, like_comment_Lyt;

                                    TextView txt_location_view;
                                    final ReactionView[] rvl = {null};//new ReactionView();

                                    SocialPostsGridContentsAdapter adapter;
//                        LinearLayout like, angry, cry, happy, wow, love,
                                    LinearLayout pastMainLayout, shareLayout, layout_location_view;

                                    // data.buffer_video = (TextView) view.findViewById(R.id.buffer_video);
                                    // data.buffer_video.setTextColor(getActivity().getResources().getColor(R.color.red));

                                    data.custom_video_viewImageview = (ImageView) iview.findViewById(R.id.custom_video_view_imageView);
                                    data.custom_video_view = (VideoView) iview.findViewById(R.id.custom_video_view);
                                    data.custom_video_view.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                                        @Override
                                        public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                                            return true;
                                        }
                                    });
                                    data.custom_video_view.setVideoPath(data.getmSocialPostContentsModelsList().get(0).getCdnUrl());

                                    millavideoplayer = (FrameLayout) iview.findViewById(R.id.custom_video_view_frame);
                                    millavideoplayer.setVisibility(View.VISIBLE);


                                    //  millavideoplayer.setVisibility(View.GONE);
                                    viewviewoverlay = (ImageView) iview.findViewById(R.id.viewviewoverlay);
                                    viewviewoverlay.setVisibility(View.VISIBLE);
                                    layout_location_view = (LinearLayout) iview.findViewById(R.id.layout_location_view);
                                    txt_location_view = (TextView) iview.findViewById(R.id.txt_location_view);
                                    commLayout = (LinearLayout) iview.findViewById(R.id.social_layout_comments);
                                    commLayout.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent intent = new Intent(getActivity(), SocialCommentActivityNew.class);
                                            //  pausePreviousVideos();
                                            intent.putExtra("post_id", data.getPostid());
                                            startActivity(intent);
                                        }
                                    });
                                    click_like = (LinearLayout) iview.findViewById(R.id.click_like);
                                    click_like.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            onClickLike(data.getPostid(),"like");
                                        }
                                    });
                                    txt_time = (TextView) iview.findViewById(R.id.txt_date);
                                    commentCount = (TextView) iview.findViewById(R.id.comment_count);
                                    txt_like_count = (TextView) iview.findViewById(R.id.txt_like_count);
                                    txt_comment = (TextView) iview.findViewById(R.id.txt_comment);
                                    txt_share = (TextView) iview.findViewById(R.id.txt_share);
                                    txt_title = (TextView) iview.findViewById(R.id.txt_title);
                                    ProfileImage = (ImageView) iview.findViewById(R.id.imgUser);
                                    txt_content = (TextView) iview.findViewById(R.id.txt_content);


                                    more_menu_layout = (LinearLayout) iview.findViewById(R.id.more_menu_layout);

                                    like_comment_Lyt = (LinearLayout) iview.findViewById(R.id.comment_reaction_layout);
                                    reactionCount = (TextView) iview.findViewById(R.id.reaction_count);
                                    reactionIcon = (ImageView) iview.findViewById(R.id.reaction_icon);
                                    icons = (RecyclerView) iview.findViewById(R.id.reaction_list_recycler);

                                    emojiesDialog = (RelativeLayout) iview.findViewById(R.id.emojies_layout);
//                        like = (LinearLayout) iview.findViewById(R.id.lyt_like_button);
//                        angry = (LinearLayout) iview.findViewById(R.id.lyt_angry_button);
//                        cry = (LinearLayout) iview.findViewById(R.id.lyt_cry_button);
//                        happy = (LinearLayout) iview.findViewById(R.id.lyt_happy_button);
//                        wow = (LinearLayout) iview.findViewById(R.id.lyt_wow_button);
//                        love = (LinearLayout) iview.findViewById(R.id.lyt_love_button);
                                    pastMainLayout = (LinearLayout) iview.findViewById(R.id.past_main_layout);
                                    shareLayout = (LinearLayout) iview.findViewById(R.id.click_share);


                                        shareLayout.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                ArrayList<SocialPostContentsModel.DataBean> array = new ArrayList<>();
                                                Log.i("postidfinding_0",data.getPostid()+" "+data.getmSocialUserPostsModel().getmSocialPostsid());
                                                for(int i =0;i<data.getmSocialPostContentsModelsList().size();i++) {
                                                    SocialPostContentsModel.DataBean datamodel = new SocialPostContentsModel.DataBean();

                                                    try{
                                                        datamodel.setCdnUrl(data.getmSocialPostContentsModelsList().get(i).getCdnUrl());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setContentType(data.getmSocialPostContentsModelsList().get(i).getContentType());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setPostId(data.getmSocialPostContentsModelsList().get(i).getPostId());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setFileName(data.getmSocialPostContentsModelsList().get(i).getFileName());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setFileType(data.getmSocialPostContentsModelsList().get(i).getFileType());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setFileUrl(data.getmSocialPostContentsModelsList().get(i).getFileUrl());
                                                    }catch (Exception e){}
                                                    try{
                                                        datamodel.setPostId(data.getPostid());
                                                    }catch (Exception e){}
                                                    Log.i("postidfinding"+i,new   Gson().toJson(data.getmSocialPostContentsModelsList())+"");
                                                    try{
                                                        array.add(datamodel);
                                                    }catch (Exception e){}
                                                }
                                                createShareMenu(array).show();
                                            }

                                    });
                                    int position = data.getI();


                                    if (data.getmSocialUserDetailsModel() != null && (data.getmSocialUserDetailsModel().getUser_name() != null) ? true : false) {
                                        txt_title.setText(data.getmSocialUserDetailsModel().getUser_name() + "");
                                        RequestOptions options = new RequestOptions().centerCrop();
                                        Glide.with(getContext()).load(data.getmSocialUserDetailsModel().getImage()).apply(options)
                                                .into(ProfileImage);
                                    }

                                    {

                                        {
                                            RequestOptions options = new RequestOptions().placeholder(R.drawable.videoimageloader).centerCrop();


                                            if (data.getmSocialPostContentsModelsList().get(0).getCdnUrl() != null) {
                                                Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getsCdnUrl()).into(data.custom_video_viewImageview);
                                            } else if (data.getmSocialPostContentsModelsList().get(0).getFileUrl() != null) {
                                                Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getFileUrl()).into(data.custom_video_viewImageview);
                                            }

                                            sethight2(data.getmSocialPostContentsModelsList(), data.custom_video_view, millavideoplayer);

                                            sethight3(data.getmSocialPostContentsModelsList(), data.custom_video_viewImageview, millavideoplayer);

                                            viewviewoverlay.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {

                                                    Log.i("viewviewoverlay","click is called");



                                                    if (data.getmSocialPostsModel().isSharedPost()) {


                                                        Intent intent = new Intent(getActivity(), SocialCommentActivityNew.class);
                                                        ///  pausePreviousVideos();
                                                        intent.putExtra("post_id", data.getmSocialPostsModel().getSharedPostID());
                                                        startActivity(intent);
                                                    } else {
                                                        Intent intent = new Intent(getActivity(), SocialCommentActivityNew.class);
                                                        ///  pausePreviousVideos();
                                                        intent.putExtra("post_id", data.getPostid());
                                                        startActivity(intent);
                                                    }

                                                    if(1==2){
                                                        if (data.getmSocialPostsModel().isSharedPost()) {
                                                            frag[0] = new SocialCommnetsActivity();
                                                            b[0] = new Bundle();
                                                            b[0].putString("post_id", data.getmSocialPostsModel().getSharedPostID());
                                                            frag[0].setArguments(b[0]);
                                                            getSocialHomeActivity().fragTransaction(frag[0]);
                                                        } else {
                                                            frag[0] = new SocialCommnetsActivity();
                                                            b[0] = new Bundle();
                                                            b[0].putString("post_id", data.getPostid());
                                                            frag[0].setArguments(b[0]);
                                                            getSocialHomeActivity().fragTransaction(frag[0]);
                                                        }
                                                    }

                                                }
                                            });


                                            //  data.buffer_video.setVisibility(View.VISIBLE);
                                            // data.buffer_video.setText("loading ...914");
                                            // millavideoplayer.setVisibility(View.VISIBLE);
                                            int pos = l.findLastVisibleItemPosition();
                                            Log.i("video", new Gson().toJson(data.getmSocialPostContentsModelsList().get(0)));
                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).apply(options).into(data.custom_video_viewImageview);
                                            //setDefaults(data.getmSocialPostsModel().getmSocialPostsid(),data.getmSocialPostContentsModelsList().get(0).getFileUrl(),getContext());

                                        }


                                    }
                                    if (data.getI() == slimedata.size() - 1) {
                                        getSocialUserPostsMilla();
                                    } else {
                                        Log.i("cdata", data.getI() + "  " + slimedata.size());
                                    }

                                    Log.i("iamvideo", "true " + data.getmSocialPostContentsModelsList().get(0).getCdnUrl());
                                }
                            });
                        }
                    }
                })
            /*    .register(R.layout.adapter_social_posts_img1, new SlimInjector<MultiImages1>() {
                    @Override
                    public void onInject(MultiImages1 data1, IViewInjector iview) {

                        if (getActivity() != null) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    MainDetails data = data1.getMainDetails();
                                    List<SocialPostCountersModel> postCountersModelList = new ArrayList<>();
                                    final Bundle[] b = new Bundle[1];
                                    final Fragment[] frag = {null};

                                    VideoView videoView;
                                    ImageView viewviewoverlay;
                                    viewviewoverlay = (ImageView) iview.findViewById(R.id.viewviewoverlay);
                                    viewviewoverlay.setVisibility(View.GONE);
                                    FrameLayout millavideoplayer;
                                    Context context = getContext();
                                    data.setMultiple(false);
                                    ClickAction actionClick;
                                    RelativeLayout emojiesDialog;
                                    ReactionListAdapter reactionListAdapter;
                                    TextView txt_time, txt_like_count, txt_comment, txt_share, txt_title, txt_content, reactionCount, commentCount;
                                    ImageView ProfileImage, reactionIcon;
                                    RecyclerView icons;


                                    LinearLayout commLayout, more_menu_layout, click_like, like_comment_Lyt;

                                    TextView txt_location_view;
                                    final ReactionView[] rvl = {null};//new ReactionView();

                                    SocialPostsGridContentsAdapter adapter;
//                        LinearLayout like, angry, cry, happy, wow, love,
                                    LinearLayout pastMainLayout, shareLayout, layout_location_view;

                                    // data.buffer_video = (TextView) view.findViewById(R.id.buffer_video);
                                    // data.buffer_video.setTextColor(getActivity().getResources().getColor(R.color.red));

                                    data.custom_video_viewImageview = (ImageView) iview.findViewById(R.id.custom_video_view_imageView);
                                    data.custom_video_viewImageview.setVisibility(View.VISIBLE);
                                            millavideoplayer = (FrameLayout) iview.findViewById(R.id.custom_video_view_frame);
                                    millavideoplayer.setVisibility(View.VISIBLE);

data.custom_video_viewImageview.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getActivity(), SocialCommentActivityNew.class);
        //  pausePreviousVideos();
        intent.putExtra("post_id", data.getPostid());
        startActivity(intent);
        Log.i("custom_vid","click is working");
    }
});

                                    layout_location_view = (LinearLayout) iview.findViewById(R.id.layout_location_view);
                                    txt_location_view = (TextView) iview.findViewById(R.id.txt_location_view);
                                    commLayout = (LinearLayout) iview.findViewById(R.id.social_layout_comments);
                                    click_like = (LinearLayout) iview.findViewById(R.id.click_like);
                                    txt_time = (TextView) iview.findViewById(R.id.txt_date);
                                    commentCount = (TextView) iview.findViewById(R.id.comment_count);
                                    txt_like_count = (TextView) iview.findViewById(R.id.txt_like_count);
                                    txt_comment = (TextView) iview.findViewById(R.id.txt_comment);
                                    txt_share = (TextView) iview.findViewById(R.id.txt_share);
                                    txt_title = (TextView) iview.findViewById(R.id.txt_title);
                                    ProfileImage = (ImageView) iview.findViewById(R.id.imgUser);
                                    txt_content = (TextView) iview.findViewById(R.id.txt_content);


                                    more_menu_layout = (LinearLayout) iview.findViewById(R.id.more_menu_layout);

                                    like_comment_Lyt = (LinearLayout) iview.findViewById(R.id.comment_reaction_layout);
                                    reactionCount = (TextView) iview.findViewById(R.id.reaction_count);
                                    reactionIcon = (ImageView) iview.findViewById(R.id.reaction_icon);
                                    icons = (RecyclerView) iview.findViewById(R.id.reaction_list_recycler);

                                    emojiesDialog = (RelativeLayout) iview.findViewById(R.id.emojies_layout);
//                        like = (LinearLayout) iview.findViewById(R.id.lyt_like_button);
//                        angry = (LinearLayout) iview.findViewById(R.id.lyt_angry_button);
//                        cry = (LinearLayout) iview.findViewById(R.id.lyt_cry_button);
//                        happy = (LinearLayout) iview.findViewById(R.id.lyt_happy_button);
//                        wow = (LinearLayout) iview.findViewById(R.id.lyt_wow_button);
//                        love = (LinearLayout) iview.findViewById(R.id.lyt_love_button);
                                    pastMainLayout = (LinearLayout) iview.findViewById(R.id.past_main_layout);
                                    shareLayout = (LinearLayout) iview.findViewById(R.id.click_share);

                                    int position = data.getI();


                                    if (data.getmSocialUserDetailsModel() != null && (data.getmSocialUserDetailsModel().getUser_name() != null) ? true : false) {
                                        txt_title.setText(data.getmSocialUserDetailsModel().getUser_name() + "");
                                        RequestOptions options = new RequestOptions().centerCrop();
                                        Glide.with(getContext()).load(data.getmSocialUserDetailsModel().getImage()).apply(options)
                                                .into(ProfileImage);
                                    }

                                    {

                                        {
                                            RequestOptions options = new RequestOptions().placeholder(R.drawable.videoimageloader).centerCrop();


                                            if (data.getmSocialPostContentsModelsList().get(0).getCdnUrl() != null) {
                                                Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getsCdnUrl()).into(data.custom_video_viewImageview);
                                            } else if (data.getmSocialPostContentsModelsList().get(0).getFileUrl() != null) {
                                                Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getFileUrl()).into(data.custom_video_viewImageview);
                                            }

                                             sethight3(data.getmSocialPostContentsModelsList(), data.custom_video_viewImageview, millavideoplayer);


                                            //  data.buffer_video.setVisibility(View.VISIBLE);
                                            // data.buffer_video.setText("loading ...914");
                                            // millavideoplayer.setVisibility(View.VISIBLE);
                                            int pos = l.findLastVisibleItemPosition();
                                            Log.i("video", new Gson().toJson(data.getmSocialPostContentsModelsList().get(0)));
                                            Glide.with(getContext()).load(data.getmSocialPostContentsModelsList().get(0).getCdnUrl()).apply(options).into(data.custom_video_viewImageview);
                                            //setDefaults(data.getmSocialPostsModel().getmSocialPostsid(),data.getmSocialPostContentsModelsList().get(0).getFileUrl(),getContext());

                                        }


                                    }
                                    if (data.getI() == slimedata.size() - 1) {
                                        getSocialUserPostsMilla();
                                    } else {
                                        Log.i("cdata", data.getI() + "  " + slimedata.size());
                                    }

                                    Log.i("iamvideo", "true " + data.getmSocialPostContentsModelsList().get(0).getCdnUrl());
                                }
                            });
                        }
                    }
                })*/
                .register(R.layout.adapter_social_posts_img1, new SlimInjector<MultiImages1>() {
                    @Override
                    public void onInject(MultiImages1 data1, IViewInjector iview) {
                        Log.i("oppo","MultiImages1 called");
                        {
                                    MainDetails data = data1.getMainDetails();
                                    List<SocialPostCountersModel> postCountersModelList = new ArrayList<>();
                                    final Bundle[] b = new Bundle[1];
                                    final Fragment[] frag = {null};

                                    ImageView viewviewoverlay;

                                    Context context = getContext();
                                    data.setMultiple(false);
                                    ClickAction actionClick;
                                    RelativeLayout emojiesDialog;
                                    ReactionListAdapter reactionListAdapter;
                                    TextView txt_time, txt_like_count, txt_comment, txt_share, txt_title, txt_content, reactionCount, commentCount;
                                    ImageView ProfileImage, reactionIcon;
                                    RecyclerView icons;


                                    LinearLayout commLayout, more_menu_layout, click_like, like_comment_Lyt;

                                    TextView txt_location_view;
                                    final ReactionView[] rvl = {null};//new ReactionView();

                                    SocialPostsGridContentsAdapter adapter;
//                        LinearLayout like, angry, cry, happy, wow, love,
                                    LinearLayout pastMainLayout, shareLayout, layout_location_view;

                                    // data.buffer_video = (TextView) view.findViewById(R.id.buffer_video);
                                    // data.buffer_video.setTextColor(getActivity().getResources().getColor(R.color.red));
                                    data.custom_video_viewImageview = (ImageView) iview.findViewById(R.id.custom_video_view_imageView);
                                    RequestOptions options = new RequestOptions().centerCrop().placeholder(R.drawable.videoimageloader);
                                    Glide.with(getContext())
                                            .load(data.getmSocialPostContentsModelsList().get(0).getsCdnUrl())
                                            .apply(options)
                                            .into(data.custom_video_viewImageview);
                                    data.custom_video_viewImageview.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            if (data.getmSocialPostsModel().isSharedPost()) {


                                                Intent intent = new Intent(getActivity(), SocialCommentActivityNew.class);
                                                ///  pausePreviousVideos();
                                                intent.putExtra("post_id", data.getmSocialPostsModel().getSharedPostID());
                                                startActivity(intent);
                                            } else {
                                                Intent intent = new Intent(getActivity(), SocialCommentActivityNew.class);
                                                ///  pausePreviousVideos();
                                                intent.putExtra("post_id", data.getPostid());
                                                startActivity(intent);
                                            }

                                           /* Intent intent = new Intent(getActivity(), SocialCommentActivityNew.class);
                                            //  pausePreviousVideos();
                                            intent.putExtra("post_id", data.getPostid());
                                            startActivity(intent);*/
                                        }
                                    });
                                            //  millavideoplayer.setVisibility(View.GONE);
                                    viewviewoverlay = (ImageView) iview.findViewById(R.id.viewviewoverlay);

                                    layout_location_view = (LinearLayout) iview.findViewById(R.id.layout_location_view);
                                    txt_location_view = (TextView) iview.findViewById(R.id.txt_location_view);
                                    commLayout = (LinearLayout) iview.findViewById(R.id.social_layout_comments);
                                    commLayout.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent intent = new Intent(getActivity(), SocialCommentActivityNew.class);
                                            //  pausePreviousVideos();
                                            intent.putExtra("post_id", data.getPostid());
                                            startActivity(intent);
                                        }
                                    });
                                    click_like = (LinearLayout) iview.findViewById(R.id.click_like);
                                    click_like.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            onClickLike(data.getPostid(),"like");
                                        }
                                    });
                                    txt_time = (TextView) iview.findViewById(R.id.txt_date);
                                    commentCount = (TextView) iview.findViewById(R.id.comment_count);
                                    txt_like_count = (TextView) iview.findViewById(R.id.txt_like_count);
                                    txt_comment = (TextView) iview.findViewById(R.id.txt_comment);
                                    txt_share = (TextView) iview.findViewById(R.id.txt_share);
                                    txt_title = (TextView) iview.findViewById(R.id.txt_title);
                                    ProfileImage = (ImageView) iview.findViewById(R.id.imgUser);
                                    txt_content = (TextView) iview.findViewById(R.id.txt_content);

                                    like_comment_Lyt = (LinearLayout) iview.findViewById(R.id.comment_reaction_layout);
                                    reactionCount = (TextView) iview.findViewById(R.id.reaction_count);
                                    reactionIcon = (ImageView) iview.findViewById(R.id.reaction_icon);
                                    icons = (RecyclerView) iview.findViewById(R.id.reaction_list_recycler);

                                    emojiesDialog = (RelativeLayout) iview.findViewById(R.id.emojies_layout);
//                        like = (LinearLayout) iview.findViewById(R.id.lyt_like_button);
//                        angry = (LinearLayout) iview.findViewById(R.id.lyt_angry_button);
//                        cry = (LinearLayout) iview.findViewById(R.id.lyt_cry_button);
//                        happy = (LinearLayout) iview.findViewById(R.id.lyt_happy_button);
//                        wow = (LinearLayout) iview.findViewById(R.id.lyt_wow_button);
//                        love = (LinearLayout) iview.findViewById(R.id.lyt_love_button);
                                    pastMainLayout = (LinearLayout) iview.findViewById(R.id.past_main_layout);
                                    shareLayout = (LinearLayout) iview.findViewById(R.id.click_share);
                                    shareLayout.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            ArrayList<SocialPostContentsModel.DataBean> array = new ArrayList<>();
                                            Log.i("postidfinding_0",data.getPostid()+" "+data.getmSocialUserPostsModel().getmSocialPostsid());
                                            for(int i =0;i<data.getmSocialPostContentsModelsList().size();i++) {
                                                SocialPostContentsModel.DataBean datamodel = new SocialPostContentsModel.DataBean();

                                                try{
                                                    datamodel.setCdnUrl(data.getmSocialPostContentsModelsList().get(i).getCdnUrl());
                                                }catch (Exception e){}
                                                try{
                                                    datamodel.setContentType(data.getmSocialPostContentsModelsList().get(i).getContentType());
                                                }catch (Exception e){}
                                                try{
                                                    datamodel.setPostId(data.getmSocialPostContentsModelsList().get(i).getPostId());
                                                }catch (Exception e){}
                                                try{
                                                    datamodel.setFileName(data.getmSocialPostContentsModelsList().get(i).getFileName());
                                                }catch (Exception e){}
                                                try{
                                                    datamodel.setFileType(data.getmSocialPostContentsModelsList().get(i).getFileType());
                                                }catch (Exception e){}
                                                try{
                                                    datamodel.setFileUrl(data.getmSocialPostContentsModelsList().get(i).getFileUrl());
                                                }catch (Exception e){}
                                                try{
                                                    datamodel.setPostId(data.getPostid());
                                                }catch (Exception e){}
                                                Log.i("postidfinding"+i,new   Gson().toJson(data.getmSocialPostContentsModelsList())+"");
                                                try{
                                                    array.add(datamodel);
                                                }catch (Exception e){}
                                            }
                                            createShareMenu(array).show();
                                        }
                                    });
                                    int position = data.getI();


                                    if (data.getmSocialUserDetailsModel() != null && (data.getmSocialUserDetailsModel().getUser_name() != null) ? true : false) {
                                        txt_title.setText(data.getmSocialUserDetailsModel().getUser_name() + "");
                                        options = new RequestOptions().centerCrop();
                                        Glide.with(getContext()).load(data.getmSocialUserDetailsModel().getImage()).apply(options)
                                                .into(ProfileImage);
                                    }

                                    if (data.getmSocialPostContentsModelsList() != null ?
                                            (data.getmSocialPostContentsModelsList().size() > 0 ? true : false) : false) {
                                        //millavideoplayer.setVisibility(View.GONE);

                                        // millavideoplayer.setVisibility(View.GONE);




                                        // TODO: 18/1/19 need to check why data is missing
                                        Log.i("content", "ismissing");
                                    }

                                    if (data.getI() == slimedata.size() - 1) {
                                        getSocialUserPostsMilla();
                                    } else {
                                        Log.i("cdata", data.getI() + "  " + slimedata.size());
                                    }

                        }
                    }

                })
                .register(R.layout.below_toolbar_header_layout, new SlimInjector<String>() {
                    @Override
                    public void onInject(String data, IViewInjector view) {

                        LinearLayout location_post_button, image_post_button, video_post_button, live_post_button;
                        video_post_button = (LinearLayout) view.findViewById(R.id.video_post_button);
                        video_post_button.setOnClickListener(v -> {
                            Log.i("video_post_button", "clicked");
                            TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(getSocialHomeActivity())
                                    .setOnMultiImageSelectedListener(uriList -> {

                                        for (int i = 0; i < uriList.size(); i++) {
                                            SocialPostContentsModel.DataBean model = new SocialPostContentsModel.DataBean();
                                            String name = Utils.getRealPathFromURI(getSocialHomeActivity(), uriList.get(i));
                                            model.setCdnUrl(name);
                                            model.setContentType("2");
                                            model.setFileType("VID");
                                            list.add(model);
                                        }
                                        if (!list.isEmpty()) {

                                            SocialAddPostFragment home = new SocialAddPostFragment();
                                            Bundle bundle = new Bundle();
                                            bundle.putSerializable("datas", list);
                                            home.setArguments(bundle);
                                            getSocialHomeActivity().addFragment(home);

                                        }
                                    })
                                    .showVideoMedia()
                                    .showTitle(false)
                                    .setSelectMaxCount(1)
                                    .setCompleteButtonText(getString(R.string.done))
                                    .setEmptySelectionText(getString(R.string.no_video_select))
                                    .create();

                            bottomSheetDialogFragment.show(getFragmentManager());

                        });

                        location_post_button = (LinearLayout) view.findViewById(R.id.location_post_button);
                        location_post_button.setOnClickListener(v -> getLocation());
                        location_post_button.setOnClickListener(v -> getLocations());

                        image_post_button = (LinearLayout) view.findViewById(R.id.image_post_button);
                        image_post_button.setOnClickListener(v -> {
                            Log.i("image_post_button", "clicked");
                            TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(getSocialHomeActivity())
                                    .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                                        @Override
                                        public void onImagesSelected(ArrayList<Uri> uriList) {


                                            for (int i = 0; i < uriList.size(); i++) {
                                                SocialPostContentsModel.DataBean model = new SocialPostContentsModel.DataBean();
                                                String name = Utils.getRealPathFromURI(getSocialHomeActivity(), uriList.get(i));
                                                Log.i("result_name_" + i, name);
                                                model.setCdnUrl(name);
                                                model.setContentType("1");
                                                model.setFileType("IMG");
                                                list.add(model);
                                            }
                                            if (!list.isEmpty()) {

                                                SocialAddPostFragment home = new SocialAddPostFragment();
                                                Bundle bundle = new Bundle();
                                                bundle.putSerializable("datas", list);
                                                home.setArguments(bundle);
                                                getSocialHomeActivity().addFragment(home);
                                                //list.clear();
//                                getFragmentManager().beginTransaction().replace(R.id.frame, home).addToBackStack(null).commit();
                                            }
                                        }
                                    })
                                    .showTitle(false)
                                    .setSelectMaxCount(6)
                                    .setCompleteButtonText(getString(R.string.done))
                                    .setEmptySelectionText(getString(R.string.no_images_select))
                                    .create();

                            bottomSheetDialogFragment.show(getFragmentManager());
                        });


                        /*video_post_button.setOnClickListener(v -> {
                            TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(getSocialHomeActivity())
                                    .setOnMultiImageSelectedListener(uriList -> {
                                        for (int i = 0; i < uriList.size(); i++) {
                                            SocialPostContentsModel.DataBean model = new SocialPostContentsModel.DataBean();
                                            String name = Utils.getRealPathFromURI(getSocialHomeActivity(), uriList.get(i));
                                            model.setCdnUrl(name);
                                            model.setContentType("2");
                                            model.setFileType("VID");
                                            list.add(model);
                                        }
                                        if (!list.isEmpty()) {
                                            SocialAddPostFragment home = new SocialAddPostFragment();
                                            Bundle bundle = new Bundle();
                                            bundle.putSerializable("datas", list);
                                            home.setArguments(bundle);
                                            getSocialHomeActivity().addFragment(home);
//                            getSocialHomeActivity().addFragment(home);
//                            getFragmentManager().beginTransaction().replace(R.id.frame, home).addToBackStack(null).commit();
                                        }
                                    })
                                    .showVideoMedia()
                                    .showTitle(false)
                                    .setSelectMaxCount(6)
                                    .setCompleteButtonText(getString(R.string.done))
                                    .setEmptySelectionText(getString(R.string.no_video_select))
                                    .create();

                            bottomSheetDialogFragment.show(getFragmentManager());

                        });*/
                        addPost = (TextView) view.findViewById(R.id.txt_write_something);
                        addPost.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SocialAddPostFragment home = new SocialAddPostFragment();
                                getSocialHomeActivity().addFragment(home);
//                getFragmentManager().beginTransaction().replace(R.id.frame, home).addToBackStack(null).commit();

                            }
                        });

                        layout_search.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SocialFriendsFragment home = new SocialFriendsFragment();
                                getSocialHomeActivity().addFragment(home);
                            }
                        });

                        live_post_button = (LinearLayout) view.findViewById(R.id.live_post_button);
                        live_post_button.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //GoLive();

                                Calendar d = Calendar.getInstance();
                                Long Channel_id = (d.getTime()).getTime();

                                Intent i = new Intent(getContext(), LiveRoomActivity.class);
                                i.putExtra(ConstantApp.ACTION_KEY_CROLE, io.agora.rtc.Constants.CLIENT_ROLE_BROADCASTER);
                                i.putExtra(ConstantApp.ACTION_KEY_ROOM_NAME, Channel_id + "");
                                i.putExtra("Social", "1");
//                i.putExtra(ConstantApp.ACTION_KEY_ROOM_NAME, modelList.get(position).getProperty_id());
                                startActivity(i);

                            }
                        });


                        CircleImageView profilepick = (CircleImageView) view.findViewById(R.id.prof_pic);

                        if (preference.getSocialImage() != null) {
                            Glide.with(getContext()).load(preference.getSocialImage()).into(profilepick);
                        }


                        RequestOptions options = new RequestOptions().centerCrop();
                        Glide.with(Objects.requireNonNull(getContext())).load(preference.getSocialImage()).apply(options).into(prof_pic);

                    }
                })
                .register(R.layout.stories_recyclerview, new SlimInjector<SocialUserFriendsBroadcastModel>() {

                    @Override
                    public void onInject(SocialUserFriendsBroadcastModel data, IViewInjector injector) {
                        // mLiveStories=new LiveStories(view,getContext(),getActivity());
                        stories = (RecyclerView) injector.findViewById(R.id.recyc_social_stories);
                        RecyclerView.LayoutManager lm = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                        ((LinearLayoutManager) lm).getStackFromEnd();
                        stories.setLayoutManager(lm);
                        stories.setItemAnimator(new DefaultItemAnimator());
                        if (myRef == null) {
                            database = FirebaseDatabase.getInstance();
                            myRef = database.getReference();
                        }
                        mStoriesAdapter = new SocialStoreisHome(getContext(), Stores_List, myRef, anInterface);
                        stories.setAdapter(mStoriesAdapter);
                        getStores();
                    }
                });

        /*rv2.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                Log.i("onScrollStateChanged", "is calling");
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:

                        String nowfullydisplayed = "";//+l.findLastCompletelyVisibleItemPosition();
                        try {
                            if (scrolingdown) {
                                nowfullydisplayed = "" + l.findLastCompletelyVisibleItemPosition();
                                int pos = l.findLastCompletelyVisibleItemPosition();
                                if (pos > 1) {





                                    try {
                                        if (mSlimAdapter.getItem(pos) instanceof VideoOnly) {
                                            VideoOnly v = (VideoOnly) mSlimAdapter.getItem(pos);
                                            MainDetails m = v.getMainDetails();
                                            if(m.custom_video_viewImageview!=null)m.custom_video_viewImageview.setVisibility(View.VISIBLE);
                                            // MainDetails m_= (MainDetails) mSlimAdapter.getItem(pos);

                                            player_session(m, pos);

                                        }
                                    } catch (Exception e) {
                                        Log.e("onScrollStateCh20232", e.getMessage() + "");

                                    }
                                } else {

                                    if (l.findFirstVisibleItemPosition() == l.findLastVisibleItemPosition()) {
                                        pos = l.findFirstVisibleItemPosition();



                                        try {
                                            if (mSlimAdapter.getItem(pos) instanceof VideoOnly) {
                                                VideoOnly v = (VideoOnly) mSlimAdapter.getItem(pos);
                                                MainDetails m = v.getMainDetails();
                                                // MainDetails m_= (MainDetails) mSlimAdapter.getItem(pos);
                                                if(m.custom_video_viewImageview!=null)m.custom_video_viewImageview.setVisibility(View.VISIBLE);

                                                player_session2(m, pos);

                                            }
                                        } catch (Exception e) {
                                            Log.e("onScrollStateCh20255", e.getMessage() + "");

                                        }
                                    } else {


                                        try {
                                            if (mSlimAdapter.getItem(pos) instanceof VideoOnly) {
                                                VideoOnly v = (VideoOnly) mSlimAdapter.getItem(pos);
                                                MainDetails m = v.getMainDetails();

                                                // MainDetails m_= (MainDetails) mSlimAdapter.getItem(pos);
                                                if(m.custom_video_viewImageview!=null)m.custom_video_viewImageview.setVisibility(View.VISIBLE);

                                                player_session2(m, pos);

                                            }
                                        } catch (Exception e) {
                                            Log.e("onScrollStateCh20274", e.getMessage() + "");

                                        }
                                    }
                                }
                            } else {


                                int pos = l.findFirstCompletelyVisibleItemPosition();

                                if (pos > 1) {



                                    try {
                                        if (mSlimAdapter.getItem(pos) instanceof VideoOnly) {
                                            VideoOnly v = (VideoOnly) mSlimAdapter.getItem(pos);
                                            MainDetails m = v.getMainDetails();
                                            if(m.custom_video_viewImageview!=null)m.custom_video_viewImageview.setVisibility(View.VISIBLE);

                                            player_session(m, pos);

                                        }
                                    } catch (Exception e) {
                                        Log.e("onScrollStateCh20297", e.getMessage() + "");

                                    }
                                } else {
                                    if (l.findFirstVisibleItemPosition() == l.findLastVisibleItemPosition()) {
                                        pos = l.findFirstVisibleItemPosition();

                                        try {
                                            if (mSlimAdapter.getItem(pos) instanceof VideoOnly) {
                                                VideoOnly v = (VideoOnly) mSlimAdapter.getItem(pos);
                                                MainDetails m = v.getMainDetails();
                                                // MainDetails m_= (MainDetails) mSlimAdapter.getItem(pos);
                                                if(m.custom_video_viewImageview!=null)m.custom_video_viewImageview.setVisibility(View.VISIBLE);

                                                player_session(m, pos);

                                            }
                                        } catch (Exception e) {
                                            Log.e("onScrollStateCh20214", e.getMessage() + "");

                                        }
                                    } else {
                                        pos = l.findLastVisibleItemPosition();

                                        try {
                                            if (mSlimAdapter.getItem(pos) instanceof VideoOnly) {
                                                VideoOnly v = (VideoOnly) mSlimAdapter.getItem(pos);
                                                MainDetails m = v.getMainDetails();
                                                // MainDetails m_= (MainDetails) mSlimAdapter.getItem(pos);
                                                if(m.custom_video_viewImageview!=null)m.custom_video_viewImageview.setVisibility(View.VISIBLE);

                                                player_session2(m, pos);

                                            }
                                        } catch (Exception e) {
                                            Log.e("onScrollStateCh32", e.getMessage() + "");

                                        }
                                    }
                                }
                                nowfullydisplayed = "" + l.findFirstCompletelyVisibleItemPosition();
                            }
                            if (scrolingdown == false) {
                                if (nowfullydisplayed.equals("0") && l.findFirstVisibleItemPosition() == 0) {
                                    try {
                                        if (mSlimAdapter.getItem(2) instanceof VideoOnly) {
                                            VideoOnly v = (VideoOnly) mSlimAdapter.getItem(2);
                                            MainDetails m = v.getMainDetails();
                                            if(m.custom_video_viewImageview!=null)m.custom_video_viewImageview.setVisibility(View.VISIBLE);

                                            player_session(m, 2);

                                        }
                                    } catch (Exception e) {
                                        Log.e("onScrollStateCh20252", e.getMessage() + "");

                                    }

                                }
                            }
                            Log.v("monday", "SCROLL_STATE_IDLE nd:" + nowfullydisplayed + "  sd:" + scrolingdown + "\n" +
                                    "   l:" + l.findLastVisibleItemPosition() + "  f:" + l.findFirstVisibleItemPosition());

                        } catch (Exception e) {
                            Log.e("onScrollStateChan2119", e.getMessage() + "");
                        }
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        Log.v("monday", "SCROLL_STATE_DRAGGING");
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        Log.v("monday", "SCROLL_STATE_SETTLING");
                        break;
                }
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dx > 0) {
                    Log.v("monday", "Scrolled Right");

                } else if (dx < 0) {
                    Log.v("monday", "Scrolled Left");
                } else {
                    Log.v("monday", "No Horizontal Scrolled");
                }

                if (dy > 0) {
                    Log.v("monday", "Scrolled Downwards");
                    scrolingdown = true;
                } else if (dy < 0) {
                    Log.v("monday", "Scrolled Upwards");
                    scrolingdown = false;
                } else {
                    Log.v("monday", "No Vertical Scrolled");
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });*/
        slimedata = new ArrayList<>();
        slimedata.add(new String("item"));
        //slimedata.add(new ProgressSpinnerMilla());
        slimedata.add(new SocialUserFriendsBroadcastModel());
        mSlimAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                Log.i("registerAdapte", positionStart + "  " + itemCount);
                super.onItemRangeInserted(positionStart, itemCount);
            }

            @Override
            public void onChanged() {
                super.onChanged();
                Log.i("registerAdapte", "  er");
            }

        });
        mCustomScrollListener = new CustomScrollListener();
        //       rv2.setNestedScrollingEnabled(false);
        mSlimAdapter.attachTo(rv2);
        // slimedata.add(new MainDetails());
        mSlimAdapter.updateData(slimedata);
        getSocialUserPostsMilla();
        return view;
    }

    private void showDialogue(ArrayList<SocialPostContentsModel.DataBean> array) {

        ListView listView = getActivity().findViewById(R.id.listview);
        final String[] items = getActivity().getResources().getStringArray(R.array.share_titles);
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, items);
        listView.setAdapter(itemsAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    SocialAddPostFragment home = new SocialAddPostFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("datas", array);
                    bundle.putString("from", "share_home");
                    home.setArguments(bundle);
                    getSocialHomeActivity().addFragment(home);
                } else {
                    String shareBody = SHAREBASEURL + array.get(0).getPostId();
                    Log.d("postId", shareBody);
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    intent.setType("String/*");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(Intent.createChooser(intent, getString(R.string.share_app_via)));
                }
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
//                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
    }

    void sethight(List<newDataBean> postcontent, VideoView mTextureVideoView, FrameLayout f1) {
        Double original_width, original_height;

        if (postcontent.get(0).getImageHeight() != null) {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            // int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            Log.i("imageheight", "is " + postcontent.get(0).getImageHeight());
            original_height = Double.parseDouble(postcontent.get(0).getImageHeight() + "");
            original_width = Double.parseDouble(postcontent.get(0).getImageWidth());
            Double ration = original_height / original_width;
            Log.i("checking_23", ration + "");
            Double newh = width * ration;
            mTextureVideoView.getLayoutParams().width = width;
            mTextureVideoView.getLayoutParams().height = newh.intValue();
            mTextureVideoView.requestLayout();

            f1.getLayoutParams().width = width;
            f1.getLayoutParams().height = newh.intValue();
            f1.requestLayout();
            Log.i("imageheight197", "is not null w=>" + width + ":h=>" + newh + "   h=>" + original_height + ":w=>" + original_width);
            //  holder.video_layout.getLayoutParams().width=width;
            ///  holder.video_layout.getLayoutParams().height=newh.intValue();
            // holder.video_layout.requestLayout();
        } else {
            Log.i("imageheight197", "is null");

        }
    }

    void sethight(List<newDataBean> postcontent, ImageView mTextureVideoView, FrameLayout f1) {
        Double original_width, original_height;

        if (postcontent.get(0).getImageHeight() != null) {

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            // int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;
            Log.i("imageheight", "is " + postcontent.get(0).getImageHeight());
            original_height = Double.parseDouble(postcontent.get(0).getImageHeight() + "");
            original_width = Double.parseDouble(postcontent.get(0).getImageWidth());
            Double ration = original_height / original_width;
            Log.i("checking_23", ration + "");
            Double newh = width * ration;
            mTextureVideoView.getLayoutParams().width = width;
            mTextureVideoView.getLayoutParams().height = newh.intValue();
            mTextureVideoView.requestLayout();

            f1.getLayoutParams().width = width;
            f1.getLayoutParams().height = newh.intValue();
            f1.requestLayout();
            Log.i("imageheight197", "is not null w=>" + width + ":h=>" + newh + "   h=>" + original_height + ":w=>" + original_width);
            //  holder.video_layout.getLayoutParams().width=width;
            ///  holder.video_layout.getLayoutParams().height=newh.intValue();
            // holder.video_layout.requestLayout();
        } else {
            Log.i("imageheight197", "is null");

        }
    }


    private void setclick(ImageView mImg1, int i, MainDetails data, String postid) {
        mImg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Log.i("iamgeclick", "called and pos is " + i);
                newDataBean models = data.getmSocialPostContentsModelsList().get(i);
                SocialPostContentsModel.DataBean model = new SocialPostContentsModel.DataBean();
                model.setContentId_new(data.getmSocialPostsModel().getmSocialPostsid());
                model.setPostId(models.getContentId_new());
                model.setAddress(models.getAddress());
                model.setCdnUrl(models.getCdnUrl());
                model.setContentType(models.getContentType());
                model.setFileName(models.getFileName());
                model.setFileType(model.getFileType());
                model.setFileUrl(models.getFileUrl());
                model.setLatitude(models.getLatitude());
                model.setLongitude(models.getLongitude());

                GalleryLargeScreenActivity fragment = new GalleryLargeScreenActivity();
                Bundle bundle = new Bundle();
                bundle.putSerializable("model", model);
                bundle.putString("user_id", data.getmSocialPostsModel().getUserID());
                bundle.putString("post_id1", data.getmSocialPostsModel().getPostId());
                bundle.putString("post_id2", data.getmSocialPostsModel().getmSocialPostsid());
                fragment.setArguments(bundle);
                getSocialHomeActivity().addFragment(fragment);
                Log.i("iamgeclick2", "called and pos is " + i);
            }
        });
    }

    private void setclick(ImageView mImg1, int i, MainDetails data) {
        mImg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 /*final Bundle[] b = new Bundle[1];
                final Fragment[] frag = {null};
                pausePreviousVideos();
                frag[0] = new SocialCommnetsActivity();
                b[0] = new Bundle();
                newDataBean models=data.getmSocialPostContentsModelsList().get(i);
                b[0].putString("post_id", data.getmSocialPostContentsModelsList().get(i).getPostId());
                Log.i("data",new Gson().toJson(data.getmSocialPostContentsModelsList().get(i)));//.getmSocialPostContentsModelsList().get(i).getPostId());
                frag[0].setArguments(b[0]);
                ((TeyaarHomeActivity) getContext()).fragTransaction(frag[0]);

  */
                //pausePreviousVideos();
                Log.i("iamgeclick", "called and pos is " + i);
                newDataBean models = data.getmSocialPostContentsModelsList().get(i);
                SocialPostContentsModel.DataBean model = new SocialPostContentsModel.DataBean();
                model.setContentId_new(data.getmSocialPostsModel().getmSocialPostsid());
                model.setPostId(models.getContentId_new());
                model.setAddress(models.getAddress());
                model.setCdnUrl(models.getCdnUrl());
                model.setContentType(models.getContentType());
                model.setFileName(models.getFileName());
                model.setFileType(model.getFileType());
                model.setFileUrl(models.getFileUrl());
                model.setLatitude(models.getLatitude());
                model.setLongitude(models.getLongitude());
                GalleryLargeScreenActivity fragment = new GalleryLargeScreenActivity();
                Bundle bundle = new Bundle();
                bundle.putString("user_id", data.getmSocialPostsModel().getUserID());
                bundle.putSerializable("model", model);
                bundle.putString("post_id2", data.getmSocialPostContentsModelsList().get(i).getPostId());

                fragment.setArguments(bundle);
                getSocialHomeActivity().addFragment(fragment);
                Log.i("iamgeclick2", "called and pos is " + i);

            }
        });

    }


    private void player_session2_old(MainDetails m, int pos) {
        pausePreviousVideos(pos);
        Log.i("player_session2", "called");
        m.custom_video_view.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                return true;
            }
        });


        if (m.custom_video_view.getVisibility() == View.GONE) {
            m.custom_video_view.setVisibility(View.VISIBLE);
        }
        try {
            if (m.custom_video_view.isPlaying()) {

                if (m.custom_video_viewImageview.getVisibility() == View.VISIBLE) {
                    m.custom_video_viewImageview.setVisibility(View.GONE);
                }

                //m.custom_video_viewImageview.setVisibility(View.GONE);
            } else {
                current_playing_post_id = m.getmSocialPostsModel().getmSocialPostsid();

                current_playing_post_id = m.getmSocialPostsModel().getmSocialPostsid();
                // m.getCustom_video_viewVideoView().setVideoPath("https://firebasestorage.googleapis.com/v0/b/social-media-54b8.appspot.com/o/RW20seconds_1.mp4?alt=media&token=399b6897-20a5-4812-bbc9-b996c57c8b0c");
                if (m.getmSocialPostContentsModelsList() != null ? m.getmSocialPostContentsModelsList().size() > 0 ? true : false : false) {
                    if (m.getmSocialPostContentsModelsList().get(0).getCdnUrl() != null ? m.getmSocialPostContentsModelsList().get(0).getCdnUrl().trim().length() > 0 ? true : false : false) {
                        Log.i("videourl1687", m.getmSocialPostContentsModelsList().get(0).getCdnUrl() + "");
                        //  m.getCustom_video_viewVideoView().setVideoPath(m.getmSocialPostContentsModelsList().get(0).getCdnUrl());

                    } else if (m.getmSocialPostContentsModelsList().get(0).getFileUrl() != null ? m.getmSocialPostContentsModelsList().get(0).getFileUrl().trim().length() > 0 ? true : false : false) {
                        Log.i("videourl1693", m.getmSocialPostContentsModelsList().get(0).getFileUrl() + "");
                        //   m.getCustom_video_viewVideoView().setVideoPath(m.getmSocialPostContentsModelsList().get(0).getFileUrl());
                    }
                }


                m.custom_video_view.setVideoPath(m.getmSocialPostContentsModelsList().get(0).getCdnUrl());
                m.custom_video_view.start();
                Log.i("player_session1790", "start called");
                m.custom_video_view.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {

                        if (i == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {

                            if (m.custom_video_viewImageview.getVisibility() == View.VISIBLE) {
                                m.custom_video_viewImageview.setVisibility(View.GONE);

                            }


                            pausePreviousVideos(pos);
                        }


                        if (i == MediaPlayer.MEDIA_INFO_VIDEO_NOT_PLAYING) {
                            if (m.custom_video_viewImageview.getVisibility() == View.GONE) {
                                m.custom_video_viewImageview.setVisibility(View.VISIBLE);
                            }

                        }
                        return true;
                    }
                });

            }
        } catch (IndexOutOfBoundsException e) {

        }
    }


    private void player_session_old(MainDetails m, int pos) {
        Log.i("player_session", "called");
        m.custom_video_view.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                return true;
            }
        });
        pausePreviousVideos(pos);

        try {
            if (m.custom_video_view.isPlaying()) {

                if (m.custom_video_viewImageview.getVisibility() == View.VISIBLE) {
                    m.custom_video_viewImageview.setVisibility(View.GONE);
                }

            } else {
                m.custom_video_view.setVisibility(View.VISIBLE);
                current_playing_post_id = m.getmSocialPostsModel().getmSocialPostsid();

                current_playing_post_id = m.getmSocialPostsModel().getmSocialPostsid();


                m.custom_video_view.setVideoPath(m.getmSocialPostContentsModelsList().get(0).getCdnUrl());
                m.custom_video_view.start();
                Log.i("player_session1859", "start called  " + m.getmSocialPostContentsModelsList().get(0).getCdnUrl());


                m.custom_video_view.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {

                        if (i == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                            if (i == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                                if (m.custom_video_viewImageview.getVisibility() == View.VISIBLE) {
                                    m.custom_video_viewImageview.setVisibility(View.GONE);
                                }
                                pausePreviousVideos(pos);

                            }

                            if (i == MediaPlayer.MEDIA_INFO_VIDEO_NOT_PLAYING) {
                                if (m.custom_video_viewImageview.getVisibility() == View.GONE) {
                                    m.custom_video_viewImageview.setVisibility(View.VISIBLE);
                                }
                                m.custom_video_view.setVisibility(View.GONE);

                            }
                        }
                        return true;
                    }
                });

            }
        } catch (IndexOutOfBoundsException e) {

        }

    }

    private void pausePreviousVideos() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (int pos = 0; pos < slimedata.size(); pos++)
                    try {
                        if (mSlimAdapter.getItem(pos) instanceof MainDetails) {
                            MainDetails m = (MainDetails) mSlimAdapter.getItem(pos);

                            m.custom_video_view.stopPlayback();


                        }
                    } catch (Exception e) {

                    }
            }
        });
    }

    private void pausePreviousVideos(int pos) {
        try {
            if (mSlimAdapter.getItem(pos - 1) instanceof VideoOnly) {
                VideoOnly v = (VideoOnly) mSlimAdapter.getItem(pos - 1);
                MainDetails m = v.getMainDetails();


               {

                    m.custom_video_view.stopPlayback();
                    m.custom_video_view=null;
                }

            }
        } catch (Exception e) {

        }
        try {
            if (mSlimAdapter.getItem(pos + 1) instanceof MainDetails) {
                VideoOnly v = (VideoOnly) mSlimAdapter.getItem(pos + 1);
                MainDetails m = v.getMainDetails();

                m.custom_video_view.stopPlayback();
                m.custom_video_view=null;


            }
        } catch (Exception e) {

        }


        try {
            if (mSlimAdapter.getItem(pos - 2) instanceof MainDetails) {
                VideoOnly v = (VideoOnly) mSlimAdapter.getItem(pos - 2);
                MainDetails m = v.getMainDetails();

                m.custom_video_view.stopPlayback();
                m.custom_video_view=null;


            }
        } catch (Exception e) {

        }
        try {
            if (mSlimAdapter.getItem(pos + 2) instanceof MainDetails) {
                VideoOnly v = (VideoOnly) mSlimAdapter.getItem(pos + 2);
                MainDetails m = v.getMainDetails();

                m.custom_video_view.stopPlayback();
                m.custom_video_view=null;

            }
        } catch (Exception e) {

        }
    }

    boolean scrolingdown = false;
    int CLICK_ACTION_THRESHOLD = 5;

    private void player_session2(MainDetails m, int pos) {
        Log.i("player_session2", "called");
        m.custom_video_view.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                return true;
            }
        });


        if (m.custom_video_view.getVisibility() == View.GONE) {
            m.custom_video_view.setVisibility(View.VISIBLE);
        }
        try {
            if (m.custom_video_view.isPlaying()) {

                if (m.custom_video_viewImageview.getVisibility() == View.VISIBLE) {
                    m.custom_video_viewImageview.setVisibility(View.GONE);
                }
                pausePreviousVideos(pos);
                //m.custom_video_viewImageview.setVisibility(View.GONE);
            } else {
                current_playing_post_id = m.getmSocialPostsModel().getmSocialPostsid();

                current_playing_post_id = m.getmSocialPostsModel().getmSocialPostsid();
                // m.getCustom_video_viewVideoView().setVideoPath("https://firebasestorage.googleapis.com/v0/b/social-media-54b8.appspot.com/o/RW20seconds_1.mp4?alt=media&token=399b6897-20a5-4812-bbc9-b996c57c8b0c");
                if (m.getmSocialPostContentsModelsList() != null ? m.getmSocialPostContentsModelsList().size() > 0 ? true : false : false) {
                    if (m.getmSocialPostContentsModelsList().get(0).getCdnUrl() != null ? m.getmSocialPostContentsModelsList().get(0).getCdnUrl().trim().length() > 0 ? true : false : false) {
                        Log.i("videourl1687", m.getmSocialPostContentsModelsList().get(0).getCdnUrl() + "");
                        //  m.getCustom_video_viewVideoView().setVideoPath(m.getmSocialPostContentsModelsList().get(0).getCdnUrl());

                    } else if (m.getmSocialPostContentsModelsList().get(0).getFileUrl() != null ? m.getmSocialPostContentsModelsList().get(0).getFileUrl().trim().length() > 0 ? true : false : false) {
                        Log.i("videourl1693", m.getmSocialPostContentsModelsList().get(0).getFileUrl() + "");
                        //   m.getCustom_video_viewVideoView().setVideoPath(m.getmSocialPostContentsModelsList().get(0).getFileUrl());
                    }
                }


                m.custom_video_view.setVideoPath(m.getmSocialPostContentsModelsList().get(0).getCdnUrl());
                m.custom_video_view.start();
                Log.i("player_session1790", "start called");
                m.custom_video_view.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {

                        if (i == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {

                            if (m.custom_video_viewImageview.getVisibility() == View.VISIBLE) {
                                m.custom_video_viewImageview.setVisibility(View.GONE);

                            }


                            pausePreviousVideos(pos);
                        }


                        if (i == MediaPlayer.MEDIA_INFO_VIDEO_NOT_PLAYING) {
                            if (m.custom_video_viewImageview.getVisibility() == View.GONE) {
                                m.custom_video_viewImageview.setVisibility(View.VISIBLE);
                            }

                        }
                        return true;
                    }
                });

            }
        } catch (IndexOutOfBoundsException e) {

        }
    }


    private void player_session(MainDetails m, int pos) {
        Log.i("player_session", "called");
        m.custom_video_view.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                return true;
            }
        });


        try {
            if (m.custom_video_view.isPlaying()) {

                if (m.custom_video_viewImageview.getVisibility() == View.VISIBLE) {
                    m.custom_video_viewImageview.setVisibility(View.GONE);
                }
                pausePreviousVideos(pos);
            } else {
                m.custom_video_view.setVisibility(View.VISIBLE);
                current_playing_post_id = m.getmSocialPostsModel().getmSocialPostsid();

                current_playing_post_id = m.getmSocialPostsModel().getmSocialPostsid();


                m.custom_video_view.setVideoPath(m.getmSocialPostContentsModelsList().get(0).getCdnUrl());
                m.custom_video_view.start();
                Log.i("player_session1859", "start called  " + m.getmSocialPostContentsModelsList().get(0).getCdnUrl());


                m.custom_video_view.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {

                        if (i == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                            if (i == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                                if (m.custom_video_viewImageview.getVisibility() == View.VISIBLE) {
                                    m.custom_video_viewImageview.setVisibility(View.GONE);
                                }
                                pausePreviousVideos(pos);

                            }

                            if (i == MediaPlayer.MEDIA_INFO_VIDEO_NOT_PLAYING) {
                                if (m.custom_video_viewImageview.getVisibility() == View.GONE) {
                                    m.custom_video_viewImageview.setVisibility(View.VISIBLE);
                                }
                                m.custom_video_view.setVisibility(View.GONE);

                            }
                        }
                        return true;
                    }
                });

            }
        } catch (IndexOutOfBoundsException e) {

        }

    }

    private boolean isAClick(float startX, float endX, float startY, float endY) {
        float differenceX = Math.abs(startX - endX);
        float differenceY = Math.abs(startY - endY);
        return !(differenceX > CLICK_ACTION_THRESHOLD/* =5 */ || differenceY > CLICK_ACTION_THRESHOLD);
    }

    boolean postLoadedhasNotComplected = true;

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    boolean newjoin = true;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);
    }


    private void init(View view) {
        list = new ArrayList<>();


        img_search = view.findViewById(R.id.img_search);
        layout_search = view.findViewById(R.id.layout_search);
        socialPost = view.findViewById(R.id.recyc_social_post);

        addPost = view.findViewById(R.id.txt_write_something);
        prof_pic = view.findViewById(R.id.prof_pic);
        /*

        location_post_button = view.findViewById(R.id.location_post_button);
        location_post_button = view.findViewById(R.id.location_post_button);
        image_post_button = view.findViewById(R.id.image_post_button);
        video_post_button = view.findViewById(R.id.video_post_button);
        live_post_button = view.findViewById(R.id.live_post_button);
         */
        progressBar = view.findViewById(R.id.progress_bar);
//        main_layout             = view.findViewById(R.id.main_layout);
        contstraint = view.findViewById(R.id.contstraint);


        homeInterface = this;
        Interface = this;
        anInterface = this;
        userPostsList = new ArrayList<>();
        postList = new ArrayList<>();
        UserDetailsList = new ArrayList<>();
        Stores_List = new ArrayList<>();


        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        socialPost.setLayoutManager(layoutManager);
        socialPost.setItemAnimator(new DefaultItemAnimator());
        mSocialStoreisHomeNewAdapter = new SocialStoreisHomeNewAdapter(Stores_List);


        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Live2 obj3 = new Live2(new LivePostCallback() {
                    @Override
                    public void onNewPost(MainDetails obj1) {
                        obj1.setI(0);

                        slimedata.add(2, obj1);
                        mSlimAdapter.updateData(slimedata);
                        mMainDetails_dumylist.add(obj1);

                    }
                });
            }
        });


        //   GetFcmData();
        Calendar d = Calendar.getInstance();
        d.add(Calendar.HOUR, -24);
        Long StartTmLng = (d.getTime()).getTime();
        String StartDate = localToGMT(getDate(StartTmLng, "yyyy-MM-dd HH:mm:ss"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date mDate = sdf.parse(StartDate);
            long timeInMilliseconds = mDate.getTime();
            StoreStart = timeInMilliseconds + "";


        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    List<MainDetails> mMainDetails_dumylist = new ArrayList<>();


    LivePost obj12;

    private void getLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Utils.hasLocationPermission(getContext(), PERMISSION_LOCATION_REQUEST, true)) {
                try {
                    openAutocompleteActivity();

                } catch (Exception e) {
                    hideLoading();

                }
            }
        } else {
            try {
                openAutocompleteActivity();
            } catch (Exception e) {
                hideLoading();

            }
        }
    }

    private void openAutocompleteActivity() {
        try {
            AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                    .build();

            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .setFilter(typeFilter)
                            .build(getSocialHomeActivity());
            startActivityForResult(intent, PICK_LOCATION_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            GoogleApiAvailability.getInstance().getErrorDialog(getActivity(),
                    e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //  pausePreviousVideos();
//        MxVideoPlayer.releaseAllVideos();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_LOCATION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openAutocompleteActivity();
                }
                return;
            }
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Place place = null;
        hideLoading();
        if (requestCode == PICK_LOCATION_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == PICK_LOCATION_REQUEST) {
                    place = PlacePicker.getPlace(getContext(), data);

                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(getSocialHomeActivity(), data);
                    Log.i(TAG, status.getStatusMessage());

                } else if (resultCode == RESULT_CANCELED) {
                    // The user canceled the operation.

                }
                try {
                    SocialAddPostFragment home = new SocialAddPostFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("images", "https://maps.googleapis.com/maps/api/staticmap?center=" + place.getAddress() + "&zoom=11&size=1024x768&maptype=roadmap&markers=color:red|" + place.getLatLng().latitude + "," + place.getLatLng().longitude + "&key=" + GOOGLELOCATIONAPIKEY);
                    bundle.putString("lat", place.getLatLng().latitude + "");
                    bundle.putString("lng", place.getLatLng().longitude + "");
                    bundle.putString("location", place.getName() + "");
                    home.setArguments(bundle);
                    getSocialHomeActivity().addFragment(home);

                } catch (Exception e) {
                }

            }
        } else if (requestCode == PICK_LOCATION_REQUEST) {
            if (resultCode == RESULT_OK) {
                place = PlaceAutocomplete.getPlace(getSocialHomeActivity(), data);
                Log.i(TAG, "Place: " + place.getName());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getSocialHomeActivity(), data);
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
            try {
                SocialAddPostFragment home = new SocialAddPostFragment();
                Bundle bundle = new Bundle();
                bundle.putString("images", "https://maps.googleapis.com/maps/api/staticmap?center=" + place.getAddress() + "&zoom=11&size=1024x768&maptype=roadmap&markers=color:red|" + place.getLatLng().latitude + "," + place.getLatLng().longitude + "&key=" + GOOGLELOCATIONAPIKEY);
                bundle.putString("lat", place.getLatLng().latitude + "");
                bundle.putString("lng", place.getLatLng().longitude + "");
                bundle.putString("location", place.getName() + "");
                home.setArguments(bundle);
                getSocialHomeActivity().addFragment(home);

            } catch (Exception e) {
            }

        }
    }


    boolean nomorepost = false;

    int callingct = 0;


    String getSocialUserPostsMilla_key = null;


    public void getSocialUserPostsMilla() {
        //loadfinished=false;
        mMainDetails.clear();
        time++;
        Log.i("getSocialUser718", "called");
        Query imagesQuery;
        if (getSocialUserPostsMilla_key == null) {
            Log.i("getSocialUser718", "called is null UserId:" + UserId);
            imagesQuery = FirebaseDatabase.getInstance().getReference("SocialUserPosts").child(UserId)
                    .orderByKey().limitToLast(22);
        } else {
            //   Long f=1547633809703;
            Log.i("getSocialUser718", "called is not null UserId:" + UserId + "  getSocialUserPostsMilla_key:" + getSocialUserPostsMilla_key);
            imagesQuery = FirebaseDatabase.getInstance().getReference("SocialUserPosts").child(UserId)
                    .orderByKey().endAt(getSocialUserPostsMilla_key).limitToLast(22);
        }

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.getValue() != null ? dataSnapshot.getChildrenCount() > 0 ? true : false : false) {

                    long count = dataSnapshot.getChildrenCount();
                    Log.i("getSocialUser718", "count " + count);
                    final int[] ct = {0};
                    List<SocialUserPostsModel> postidlist = new ArrayList<>();
                    boolean first = false;
                    for (DataSnapshot data : dataSnapshot.getChildren()) {


                        if (first == false) {
                            getSocialUserPostsMilla_key = data.getKey();
                            first = true;
                        } else {
                            String postid = data.getKey();
                            SocialUserPostsModel s1 = data.getValue(SocialUserPostsModel.class);
                            Log.i("result456", postidlist.size() + " " + new Gson().toJson(data.getValue()) + " ");
                            s1.setmSocialPostsid(postid);
                            postidlist.add(s1);
                            Log.i("mMainDetailsf4", data.getKey() + "");
                        }
                    }
                    Log.i("mMainDetails", "GetFcmData ps:" + postidlist.size());
                    getSocialPostsMilla(postidlist);
                } else {
                    postLoadedhasNotComplected = false;
                    Log.i("getSocialUser718", "count is zero");
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };


        imagesQuery.addListenerForSingleValueEvent(valueEventListener);


    }


    public void getSocialPostsMilla(List<SocialUserPostsModel> postidlist1) {
        //List<SocialUserPostsModel> userPostsList;
        //List<SocialPostsModel> postList;


        final int[] ct1 = {0};
        for (int s = 0; s < postidlist1.size(); s++) {
            Log.i("mMainDetails567" + "s" + postidlist1.size(), "getSocialPosts called " + postidlist1.get(s).getmSocialPostsid());

            int finalS = s;
            myRef.child("SocialPosts").child(postidlist1.get(s).getmSocialPostsid())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            Log.i("mMainDetails761", "getSocialPosts complected ct:" + ct1[0] + "  pls:" + postidlist1.size());
                            //SocialPostsModel obj1=dataSnapshot.getValue(SocialPostsModel.class);
                            try {
                                MainDetails objm1 = new MainDetails();
                                objm1.setS1(true);                     //setmSocialPostsid
                                objm1.postid = dataSnapshot.getKey();// postidlist1.get(finalS).getmSocialPostsid();

                                objm1.mSocialPostsModel = dataSnapshot.getValue(SocialPostsModel.class);
                                objm1.mSocialPostsModel.setmSocialPostsid(postidlist1.get(finalS).getmSocialPostsid());
                                objm1.setSocialUserPosts(postidlist1.get(finalS));
                                mMainDetails.add(objm1);
                                if (ct1[0] == postidlist1.size() - 1) {
                                    Log.i("mMainDetails" + callingct, "SocialPosts size matched");
                                    getSocialUsers(postidlist1);
                                } else {
                                    Log.i("mMainDetails" + callingct, "SocialPosts size not matched");
                                }
                            } catch (Exception e) {

                            }
                            ct1[0]++;
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                            MainDetails objm1 = new MainDetails();
                            objm1.postid = postidlist1.get(finalS).getmSocialPostsid();
                            mMainDetails.add(objm1);
                            Log.i("mMainDetails" + callingct, "SocialPosts getSocialPosts complected " + ct1[0]);
                            if (ct1[0] == postidlist1.size() - 1) {
                                getSocialUsers(postidlist1);
                            }
                            ct1[0]++;
                            Log.e("DatabaseError", databaseError + "");
                        }
                    });


        }

    }


    List<MainDetails> mMainDetails = new ArrayList<>();

    int time = -1;

    public void getSocialUsers(List<SocialUserPostsModel> postsModelList) {

        Log.i("mMainDetails898", "getSocialUsers called");

        final int[] ct = {0};
        for (int q = 0; q < (postsModelList.size()); q++) {
            int finalQ = q;
            myRef.child("SocialUsers").child(postsModelList.get(q).getPostedUserID())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            int pos = finalQ;


                            try {
                                mMainDetails.get(pos).mSocialUserDetailsModel = dataSnapshot.getValue(SocialUserDetailsModel.class);
                                mMainDetails.get(pos).mSocialUserDetailsModel.setPushKey(dataSnapshot.getKey());
                                mMainDetails.get(pos).setS2(true);
                            } catch (IndexOutOfBoundsException e) {

                            }


                            ct[0]++;
                            Log.i("milla", "getSocialUsers reached " + ct[0]);
                            if (ct[0] == postsModelList.size() - 1) {
                                Log.i("mMainDetails" + callingct, "getSocialUsers size matched");

                                getSocialPostContents(postsModelList);
                            } else {
                                Log.i("mMainDetails" + callingct, "getSocialUsers size !matched");
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            ct[0]++;
                            Log.i("milla", "getSocialUsers reached  " + ct[0]);
                            if (ct[0] == postsModelList.size() - 1) {
                                getSocialPostContents(postsModelList);
                            }
                        }
                    });
        }
    }

    public void getSocialPostContents(List<SocialUserPostsModel> postsModelList) {
        final int[] ct = {0};
        Log.i("mMainDetails936", "getSocialPostContents called");

        for (int q = 0; q < postsModelList.size(); q++) {


            int finalQ1 = q;
            myRef.child("SocialPostContents").child(postsModelList.get(q).getmSocialPostsid()).
                    addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            int pos;
                            pos = finalQ1;

                            try {
                                mMainDetails.get(pos).setS3(true);
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    newDataBean obj1 = snapshot.getValue(newDataBean.class);
                                    obj1.setPostId(snapshot.getKey());
                                    obj1.setContentId_new(snapshot.getKey());
                                    obj1.setPushKey(snapshot.getKey());

                                    mMainDetails.get(pos).mSocialPostContentsModelsList.add(obj1);
                                }
                            } catch (IndexOutOfBoundsException e) {

                            }
                            ct[0]++;
                            Log.i("milla", "getSocialPostConents reached_c " + ct[0]);

                            if (ct[0] == postsModelList.size() - 1) {
                                getSocialPostCounters(postsModelList);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            ct[0]++;
                            Log.i("milla", "getSocialPostConents reached_e " + ct[0]);
                            if (ct[0] == mMainDetails.size() - 1) {
                                getSocialPostCounters(postsModelList);
                            }
                        }
                    });
        }
    }


    public void getSocialPostCounters(List<SocialUserPostsModel> postsModelList) {
        Log.i("mMainDetails976", "getSocialPostContents called");

        Log.i("milla", "getSocialPostCounters called ");//+ct[0]);
        final int[] ct = {0};
        for (int q = 0; q < postsModelList.size(); q++) {
            int finalQ = q;
            myRef.child("SocialPostCounters").child(postsModelList.get(q).getmSocialPostsid()).addListenerForSingleValueEvent(
                    new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            int pos;
                            pos = finalQ;


                            if (mMainDetails.size() > pos) {

                                // getSocialUserPostsMilla_key=postsModelList.get(0).getmSocialPostsid();
                                mMainDetails.get(pos).mSocialPostCountersModel = dataSnapshot.getValue(SocialPostCountersModel.class);
                                ct[0]++;
                                mMainDetails.get(pos).setS4(true);
                                Log.i("milla", "getSocialPostCounters reached " + ct[0]);
                                if (ct[0] == postsModelList.size() - 1) {
                                    getSocialPostReactions();
                                }
                            } else {
                                getSocialPostReactions();
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            ct[0]++;
                            Log.i("milla", "getSocialPostCounters reached " + ct[0]);
                            if (ct[0] == postsModelList.size() - 1) {
                                getSocialPostReactions();
                            }
                        }
                    });
        }
    }

    boolean loadmorecalled = false;

    int listsize = 11;
    boolean loadmoreenabled = false;

    public void getSocialPostReactions() {

        Collections.reverse(mMainDetails);

        int s = slimedata.size();
        for (int j = 0; j < mMainDetails.size(); j++) {
            {

                mMainDetails.get(j).setI(s + j + 1);
                mMainDetails_dumylist.add(mMainDetails.get(j));
                // slimedata.add(mMainDetails.get(j));

                if (mMainDetails.get(j).getmSocialPostContentsModelsList().size() == 3) {
                    MultiImages3 m = new MultiImages3();
                    m.setMainDetails(mMainDetails.get(j));
                    slimedata.add(m);
                } else if (mMainDetails.get(j).getmSocialPostContentsModelsList().size() == 5) {
                    MultiImages5 m = new MultiImages5();
                    m.setMainDetails(mMainDetails.get(j));
                    slimedata.add(m);
                } else if (mMainDetails.get(j).getmSocialPostContentsModelsList().size() == 6) {
                    MultiImages6 m = new MultiImages6();
                    m.setMainDetails(mMainDetails.get(j));
                    slimedata.add(m);
                } else if (mMainDetails.get(j).getmSocialPostContentsModelsList().size() == 1 ? mMainDetails.get(j).getmSocialPostContentsModelsList().get(0).getContentType().equals("1") ? true : false : false) {
                    MultiImages1 m = new MultiImages1();
                    m.setMainDetails(mMainDetails.get(j));
                    slimedata.add(m);
                } else if (mMainDetails.get(j).getmSocialPostContentsModelsList().size() == 1 ? mMainDetails.get(j).getmSocialPostContentsModelsList().get(0).getContentType().equals("2") ? true : false : false) {
                    VideoOnly m = new VideoOnly();
                    m.setMainDetails(mMainDetails.get(j));
                    slimedata.add(m);
                } else {
                   // slimedata.add(mMainDetails.get(j));
                }
                mMainDetails_dumylist.add(mMainDetails.get(j));
                // mSlimAdapter.notifyDataSetChanged();//(slimedata);3
                mSlimAdapter.updateData(slimedata);
                // mSlimAdapter.notifyDataSetChanged();//(slimedata);
            }
        }
        Log.i("result1067", mMainDetails.size() + "  " + slimedata.size());
        if (loadmorecalled == false)


            loadmorecalled = true;
        mSlimAdapter.updateData(slimedata);

    }


    public void getStores() {
        Log.i("mStoriesAdapter", "getStores called");
        myRef.child(DataTableUserStores).child(Constants.FCM_USER_ID).orderByKey().startAt(StoreStart).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Stores_List.clear();
                Log.e("DataTableUserStores", dataSnapshot + "");

                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    for (DataSnapshot dataNew : data.getChildren()) {
                        SocialUserFriendsBroadcastModel model = new SocialUserFriendsBroadcastModel();
                        Gson gson = new Gson();
                        JsonElement jsonElement = gson.toJsonTree(dataNew.getValue());
                        model = gson.fromJson(jsonElement, SocialUserFriendsBroadcastModel.class);
                        model.setKey(dataNew.getKey());
                        Stores_List.add(model);
                        // slimedata.add(model);
                        // mSlimAdapter.updateData(slimedata);
                    }


                }
                Collections.reverse(Stores_List);
                if (mLiveStories != null) {
                    mLiveStories.setStories(Stores_List);
                }
                // mSocialStoreisHomeNewAdapter.notifyDataSetChanged();
                if (mStoriesAdapter != null) {
                    Log.i("mStoriesAdapter", "is not null");
                    mStoriesAdapter.notifyDataSetChanged();
                } else {
                    Log.i("mStoriesAdapter", "is null");
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static String localToGMT(Date date) {
        try {
//        Date gmt = new Date(date.getTime() + TimeZone.getTimeZone("UTC").getOffset(date.getTime()));
//            Date date = (Date) new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dates);
            Long s = date.getTime();
            Long gmt = TimeZoneConv.toUTC(date.getTime(), Calendar.getInstance().getTimeZone());
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(gmt);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return df.format(cal.getTime());
        } catch (Exception e) {
            return null;
        }

    }


    @Override
    public void onClickLike(int position1, String PostId, String reaction) {
        try {
            myRef.child("SocialPostReactions").child(PostId).child(Constants.FCM_USER_ID).child("reaction").setValue(reaction)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
//                            Toast.makeText(getContext(), "Success", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e("onFailure", "onFailure" + e);
                        }
                    });
        } catch (Exception e) {
            Log.e("onFailure", "onFailure" + e);
        }

    }
    public void onClickLike( String PostId, String reaction) {
        try {
            myRef.child("SocialPostReactions").child(PostId).child(Constants.FCM_USER_ID).child("reaction").setValue(reaction)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.i("onClickLike3408","Success");
//                            Toast.makeText(getContext(), "Success", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e("onFailure", "onFailure" + e);
                        }
                    });
        } catch (Exception e) {
            Log.e("onFailure", "onFailure" + e);
        }

    }
    @Override
    public void onClickEmojis(int position, String PostId, View view, String Emojis) {
        switch (Emojis) {
            case "like":
                this.onClickLike(position, PostId, "like");
                break;
            case "angry":
                this.onClickLike(position, PostId, "angry");
                break;
            case "sad":
                this.onClickLike(position, PostId, "sad");
                break;
            case "happy":
                this.onClickLike(position, PostId, "happy");
                break;
            case "wow":
                this.onClickLike(position, PostId, "wow");
                break;
            case "love":
                this.onClickLike(position, PostId, "love");
                break;
            default:
                this.onClickLike(position, PostId, "like");
                break;
        }
    }

    @Override
    public void onMenuClick(int reqCode, int pos, String PostId, String userId) {
        if (reqCode == 101) {
            createMenu(PostId, userId).show();
        }
    }


    @Override
    public void onClick(String PostId, String UserName, String profilePic, String VideoUrl) {

    }

    public static Date getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return calendar.getTime();
    }


    Dialog createMenu(String postId, String userId) {
        try {
            alertDialog = new Dialog(getContext(), R.style.DialogSlideAnim);
            alertDialog.setContentView(R.layout.dialog_post_menu);
            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            Window window = alertDialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.BOTTOM;
            window.setAttributes(wlp);
            alertDialog.setCancelable(true);
            LinearLayout deleteLyt, ReportLyt;
            deleteLyt = alertDialog.findViewById(R.id.delete_post);
            ReportLyt = alertDialog.findViewById(R.id.report_post);

            if (userId.equals(UserId)) {
                ReportLyt.setVisibility(View.GONE);
            } else {
                deleteLyt.setVisibility(View.GONE);
            }

            ReportLyt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                    createReportDialog().show();
                }
            });
            deleteLyt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myRef.child("SocialPosts").child(postId).removeValue();
                    alertDialog.dismiss();
                }
            });
            // builder.create().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
            return alertDialog;
        } catch (Exception e) {
            Log.e(TAG, "createDialogue: " + e.getMessage());
        }
        return null;
    }

    Dialog createShareMenu(ArrayList<SocialPostContentsModel.DataBean> array) {
        try {
            alertDialog = new Dialog(getContext(), R.style.DialogSlideAnim);
            alertDialog.setContentView(R.layout.bottombar_layout);
            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            Window window = alertDialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.BOTTOM;
            window.setAttributes(wlp);
            alertDialog.setCancelable(true);
            LinearLayout deleteLyt, ReportLyt;
            deleteLyt = alertDialog.findViewById(R.id.delete_post);
            ReportLyt = alertDialog.findViewById(R.id.report_post);

            ListView listView = alertDialog.findViewById(R.id.listview);
            final String[] items = getActivity().getResources().getStringArray(R.array.share_titles);
            ArrayAdapter<String> itemsAdapter =
                    new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, items);
            listView.setAdapter(itemsAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {

                        SocialAddPostFragment home = new SocialAddPostFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("datas", array);
                        bundle.putString("from", "share_home");
                        home.setArguments(bundle);
                        getSocialHomeActivity().addFragment(home);

                    } else {

                        String shareBody = SHAREBASEURL + array.get(0).getPostId();
                        Log.d("postId", shareBody);
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.putExtra(Intent.EXTRA_TEXT, shareBody);
                        intent.setType("String/*");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(Intent.createChooser(intent, getString(R.string.share_app_via)));
                    }
                    alertDialog.dismiss();
                }
            });
            // builder.create().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.MATCH_PARENT);
            return alertDialog;
        } catch (Exception e) {
            Log.e(TAG, "createDialogue: " + e.getMessage());
        }
        return null;
    }

    Dialog createReportDialog() {

        try {
            alertDialog = new Dialog(getContext(), R.style.DialogSlideAnim);
            alertDialog.setContentView(R.layout.dialog_report_post);
            alertDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            Window window = alertDialog.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.BOTTOM;
            window.setAttributes(wlp);

            alertDialog.setCancelable(true);

            LinearLayout close, send;
            close = alertDialog.findViewById(R.id.close);
            send = alertDialog.findViewById(R.id.report_send);
            report_nudity = alertDialog.findViewById(R.id.report_nudity);
            report_reportsuicide = alertDialog.findViewById(R.id.report_suicide);
            report_terrorism = alertDialog.findViewById(R.id.report_terrorism);
            report_sale = alertDialog.findViewById(R.id.report_sale);
            report_speech = alertDialog.findViewById(R.id.report_speech);
            report_false = alertDialog.findViewById(R.id.report_false);
            report_spam = alertDialog.findViewById(R.id.report_spam);
            report_else = alertDialog.findViewById(R.id.report_else);
            report_harrasment = alertDialog.findViewById(R.id.report_harrasment);
            report_violence = alertDialog.findViewById(R.id.report_violence);
            report_info = alertDialog.findViewById(R.id.report_info);
            changeBacground(close, postionReport);

            report_info.setOnClickListener(v -> changeBacground(report_info, "0"));
            report_reportsuicide.setOnClickListener(v -> changeBacground(report_reportsuicide, "0"));
            report_nudity.setOnClickListener(v -> changeBacground(report_nudity, "0"));
            report_violence.setOnClickListener(v -> changeBacground(report_violence, "0"));
            report_harrasment.setOnClickListener(v -> changeBacground(report_harrasment, "0"));
            report_spam.setOnClickListener(v -> changeBacground(report_spam, "0"));
            report_false.setOnClickListener(v -> changeBacground(report_false, "0"));
            report_speech.setOnClickListener(v -> changeBacground(report_speech, "0"));
            report_sale.setOnClickListener(v -> changeBacground(report_sale, "0"));
            report_else.setOnClickListener(v -> changeBacground(report_else, "0"));
            report_terrorism.setOnClickListener(v -> changeBacground(report_terrorism, "0"));

            close.setOnClickListener(v -> {
                alertDialog.dismiss();
            });
            send.setOnClickListener(v -> {
                if (postionReport.equals("0")) {
                    alertDialog.dismiss();
                } else {
                    Toast.makeText(getContext(), R.string.reported_successfully, Toast.LENGTH_SHORT).show();
                    alertDialog.dismiss();
                }
            });

            return alertDialog;
        } catch (Exception e) {
            Log.e(TAG, "createDialogue: " + e.getMessage());
        }
        return null;
    }

    void changeBacground(View view, String pos) {

        report_nudity.setBackground(getContext().getResources().getDrawable(R.drawable.round_line_back));
        report_violence.setBackground(getContext().getResources().getDrawable(R.drawable.round_line_back));
        report_harrasment.setBackground(getContext().getResources().getDrawable(R.drawable.round_line_back));
        report_reportsuicide.setBackground(getContext().getResources().getDrawable(R.drawable.round_line_back));
        report_false.setBackground(getContext().getResources().getDrawable(R.drawable.round_line_back));
        report_speech.setBackground(getContext().getResources().getDrawable(R.drawable.round_line_back));
        report_spam.setBackground(getContext().getResources().getDrawable(R.drawable.round_line_back));
        report_sale.setBackground(getContext().getResources().getDrawable(R.drawable.round_line_back));
        report_else.setBackground(getContext().getResources().getDrawable(R.drawable.round_line_back));
        report_info.setBackground(getContext().getResources().getDrawable(R.drawable.round_line_back));
        report_terrorism.setBackground(getContext().getResources().getDrawable(R.drawable.round_line_back));

        switch (view.getId()) {
            case R.id.report_nudity:
                report_nudity.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                postionReport = "1";
                break;
            case R.id.report_violence:
                report_violence.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                postionReport = "2";
                break;
            case R.id.report_harrasment:
                report_harrasment.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                postionReport = "3";
                break;
            case R.id.report_suicide:
                report_reportsuicide.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                postionReport = "4";
                break;
            case R.id.report_sale:
                report_sale.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                postionReport = "5";
                break;
            case R.id.report_spam:
                report_spam.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                postionReport = "6";
                break;
            case R.id.report_speech:
                report_speech.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                postionReport = "7";
                break;
            case R.id.report_false:
                report_false.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                postionReport = "8";
                break;
            case R.id.report_info:
                report_info.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                postionReport = "9";
                break;
            case R.id.report_else:
                report_else.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                postionReport = "10";
                break;
            case R.id.report_terrorism:
                report_terrorism.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                postionReport = "11";
                break;
            default:
                break;

        }
        switch (pos) {
            case "1":
                report_nudity.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                break;
            case "2":
                report_violence.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                break;
            case "3":
                report_harrasment.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                break;
            case "4":
                report_reportsuicide.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                break;
            case "5":
                report_sale.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                break;
            case "6":
                report_spam.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                break;
            case "7":
                report_speech.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                break;
            case "8":
                report_false.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                break;
            case "9":
                report_info.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                break;
            case "10":
                report_else.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                break;
            case "11":
                report_terrorism.setBackground(getContext().getResources().getDrawable(R.drawable.ellips_background_yellow));
                break;
            default:
                break;

        }

    }

    private void getLocations() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Utils.hasLocationPermission(getContext(), PERMISSION_LOCATION_REQUEST, true)) {
                try {
                    showLoading(getContext());
                    isLoactionClicked = true;
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    startActivityForResult(builder.build(getSocialHomeActivity()), PICK_LOCATION_REQUEST);
                } catch (Exception e) {
                    hideLoading();
                    isLoactionClicked = false;
                }
            }
        } else {
            try {
                showLoading(getContext());
                isLoactionClicked = true;
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                startActivityForResult(builder.build(getSocialHomeActivity()), PICK_LOCATION_REQUEST);
            } catch (Exception e) {
                hideLoading();

                isLoactionClicked = false;
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    LiveStories mLiveStories;


}

interface LivePostCallback {
    public void onNewPost(MainDetails obj1);
}

class LivePost {
    FirebaseDatabase database;
    FirebaseAuth mAuth;
    DatabaseReference myRef;
    String UserId;
    boolean iamfirst = false;
    LivePostCallback mLivePostCallback;

    public LivePost(LivePostCallback mLivePostCallback) {
        this.mLivePostCallback = mLivePostCallback;
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference();
        mAuth = FirebaseAuth.getInstance();
        UserId = mAuth.getCurrentUser().getUid();
    }

    List<MainDetails> mMainDetailslive = new ArrayList<>();

    public void newpostlisiner() {
        mMainDetailslive.clear();

        Log.i("lives1", "newpostlisinercalled");
        Query imagesQuery;

        {
            Log.i("lives1_67", "newpostlisinercalled is null UserId:" + UserId);
            imagesQuery = FirebaseDatabase.getInstance().getReference("SocialUserPosts").child(UserId)
                    .orderByKey().limitToLast(1);
        }
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (iamfirst == false) {
                    iamfirst = true;
                } else {
                    List<SocialUserPostsModel> postidlist = new ArrayList<>();
                    String postid = dataSnapshot.getKey();

                    SocialUserPostsModel s1 = dataSnapshot.getValue(SocialUserPostsModel.class);

                    s1.setmSocialPostsid(postid);
                    postidlist.add(s1);
                    try {
                        if (postidlist != null ? postidlist.size() > 0 ? true : false : false) {
                            getSocialPostsMillaLIVE(postidlist);
                        }
                    } catch (Exception e) {
                        Log.e("lives1_87", e.getMessage() + "");
                    }
                    Log.i("lives1onChildAdded", new Gson().toJson(dataSnapshot.getValue()));
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        imagesQuery.addChildEventListener(childEventListener);
    }

    public void getSocialPostsMillaLIVE(List<SocialUserPostsModel> postidlist1) {
        //List<SocialUserPostsModel> userPostsList;
        //List<SocialPostsModel> postList;getmSocialPostsModel


        final int[] ct1 = {0};
        for (int s = 0; s < postidlist1.size(); s++) {
            Log.i("lives1_88" + "s" + postidlist1.size(), "getSocialPosts called " + postidlist1.get(s).getmSocialPostsid());

            int finalS = s;
            myRef.child("SocialPosts").child(postidlist1.get(s).getmSocialPostsid())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            Log.i("lives1_30", "getSocialPosts complected ct:" + ct1[0] + "  pls:" + postidlist1.size());
                            //SocialPostsModel obj1=dataSnapshot.getValue(SocialPostsModel.class);
                            MainDetails objm1 = new MainDetails();
                            objm1.setS1(true);                     //setmSocialPostsid
                            objm1.postid = dataSnapshot.getKey();// postidlist1.get(finalS).getmSocialPostsid();

                            objm1.mSocialPostsModel = dataSnapshot.getValue(SocialPostsModel.class);
                            objm1.mSocialPostsModel.setmSocialPostsid(postidlist1.get(finalS).getmSocialPostsid());
                            objm1.setSocialUserPosts(postidlist1.get(finalS));
                            mMainDetailslive.add(objm1);
                            if (ct1[0] == postidlist1.size() - 1) {
                                // Log.i("mMainDetails"+callingct,"SocialPosts size matched");
                                try {
                                    getSocialUsersLIVE(postidlist1);
                                } catch (Exception e) {
                                    Log.e("live864", e.getMessage() + "");
                                }
                            } else {
                                // Log.i("mMainDetails"+callingct,"SocialPosts size not matched");
                            }
                            ct1[0]++;
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                            MainDetails objm1 = new MainDetails();
                            objm1.postid = postidlist1.get(finalS).getmSocialPostsid();
                            mMainDetailslive.add(objm1);
                            //  Log.i("mMainDetails"+callingct, "SocialPosts getSocialPosts complected " + ct1[0]);
                            if (ct1[0] == postidlist1.size()) {
                                try {
                                    getSocialUsersLIVE(postidlist1);
                                } catch (Exception e) {
                                    Log.e("live864", e.getMessage() + "");
                                }
                            }
                            ct1[0]++;
                            Log.e("DatabaseError", databaseError + "");
                        }
                    });


        }

    }

    public void getSocialUsersLIVE(List<SocialUserPostsModel> postsModelList) {

        Log.i("mMainDetails1064", "getSocialUsers called ");

        final int[] ct = {0};
        for (int q = 0; q < (postsModelList.size()); q++) {
            int finalQ = q;
            Log.i("mMainDetails1064_2", "getSocialUsers called " + postsModelList.get(q).getPostedUserID());
            myRef.child("SocialUsers").child(postsModelList.get(q).getPostedUserID())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            int pos = finalQ;


                            mMainDetailslive.get(pos).mSocialUserDetailsModel = dataSnapshot.getValue(SocialUserDetailsModel.class);
                            mMainDetailslive.get(pos).mSocialUserDetailsModel.setPushKey(dataSnapshot.getKey());
                            mMainDetailslive.get(pos).setS2(true);


                            ct[0]++;
                            Log.i("milla1082", "getSocialUsers reached " + ct[0] + "   " + postsModelList.size());
                            if (ct[0] == postsModelList.size()) {
                                //Log.i("mMainDetails"+callingct,"getSocialUsers size matched");

                                getSocialPostContentsLIVE(postsModelList);
                            } else {
                                // Log.i("mMainDetails"+callingct,"getSocialUsers size !matched");

                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            ct[0]++;
                            Log.i("milla1096", "getSocialUsers reached  " + ct[0]);
                            if (ct[0] == postsModelList.size() - 1) {
                                getSocialPostContentsLIVE(postsModelList);
                            }
                        }
                    });
        }

    }

    public void getSocialPostContentsLIVE(List<SocialUserPostsModel> postsModelList) {
        final int[] ct = {0};
        Log.i("mMainDetails1107", "getSocialPostContents called");

        for (int q = 0; q < postsModelList.size(); q++) {


            int finalQ1 = q;
            myRef.child("SocialPostContents").child(postsModelList.get(q).getmSocialPostsid()).
                    addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            int pos;
                            pos = finalQ1;

                            try {
                                mMainDetailslive.get(pos).setS3(true);
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    newDataBean obj1 = snapshot.getValue(newDataBean.class);
                                    obj1.setPushKey(snapshot.getKey());

                                    mMainDetailslive.get(pos).mSocialPostContentsModelsList.add(obj1);
                                }
                            } catch (IndexOutOfBoundsException e) {

                            }
                            ct[0]++;
                            Log.i("milla113", "getSocialPostConents reached_c " + ct[0]);

                            if (ct[0] == postsModelList.size()) {
                                getSocialPostCountersLIVE(postsModelList);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            ct[0]++;
                            Log.i("milla", "getSocialPostConents reached_e " + ct[0]);
                            if (ct[0] == mMainDetailslive.size() - 1) {
                                getSocialPostCountersLIVE(postsModelList);
                            }
                        }
                    });
        }
    }

    public void getSocialPostCountersLIVE(List<SocialUserPostsModel> postsModelList) {
        Log.i("mMainDetails1202", "getSocialPostContents called");

        Log.i("milla", "getSocialPostCounters called ");//+ct[0]);
        final int[] ct = {0};
        for (int q = 0; q < postsModelList.size(); q++) {
            int finalQ = q;
            myRef.child("SocialPostCounters").child(postsModelList.get(q).getmSocialPostsid()).addListenerForSingleValueEvent(
                    new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            int pos;
                            pos = finalQ;


                            if (mMainDetailslive.size() > pos) {

                                // getSocialUserPostsMilla_key=postsModelList.get(0).getmSocialPostsid();
                                mMainDetailslive.get(pos).mSocialPostCountersModel = dataSnapshot.getValue(SocialPostCountersModel.class);
                                ct[0]++;
                                mMainDetailslive.get(pos).setS4(true);
                                Log.i("milla", "getSocialPostCounters reached " + ct[0]);
                                if (ct[0] == postsModelList.size()) {
                                    getSocialPostReactionsLIVE();
                                }
                            } else {
                                getSocialPostReactionsLIVE();
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            ct[0]++;
                            Log.i("milla", "getSocialPostCounters reached " + ct[0]);
                            if (ct[0] == postsModelList.size() - 1) {
                                getSocialPostReactionsLIVE();
                            }
                        }
                    });
        }
    }

    public void getSocialPostReactionsLIVE() {

        Log.i("resultLIVE1293", "getSocialPostReactionsLIVE reached " + mMainDetailslive.size());
        Collections.reverse(mMainDetailslive);
        Log.i("getSocialPostRea1301", "forloop started");
        for (int j = 0; j < mMainDetailslive.size(); j++) {
            if (mLivePostCallback != null) {
                mLivePostCallback.onNewPost(mMainDetailslive.get(j));

            }
            /*
            boolean f=false;
            for (int i = 0; i < mSocialPostsNewAdapter.mMainDetails.size(); i++) {

                if(mMainDetailslive.get(j).getPostid().equals(mSocialPostsNewAdapter.mMainDetails.get(i).getPostid())){
                    f=true;
                }
            }
            if(f==false)
            {
                Log.i("getSocialPostRea1301","false");
                mSocialPostsNewAdapter.mMainDetails.add(0,mMainDetailslive.get(j));
                mSocialPostsNewAdapter.notifyItemInserted(0);

            }else{
                Log.i("getSocialPostRea1306","true");
            }
             */


        }
        mMainDetailslive.clear();
        Log.i("getSocialPostRea1301", "forloop stoped");


        //if (mMainDetails.size() <= listsize)
        {
            //  mSocialPostsNewAdapter.notifyDataSetChanged();
        } //else
        {

            //  mSocialPostsNewAdapter.notifyItemInserted(mMainDetails.size() % listsize);

        }


    }

}

class LiveStories {
    List<TextView> txt_name_1 = new ArrayList<>();
    List<ImageView> circleImageView2_1 = new ArrayList<>(), thumb_mail_1 = new ArrayList<>();
    List<ConstraintLayout> main_layout_stores_1 = new ArrayList<>();
    List<View> item = new ArrayList<>();
    List<Integer> storiesidlist = new ArrayList<>();
    Context mContext;
    Activity mActivity;
    List<DatabaseReference> myRef = new ArrayList<>();
    List<FirebaseDatabase> databaseList = new ArrayList<>();

    public LiveStories(View view, Context mContext,
                       Activity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        storiesidlist.add(R.id.storie1);
        storiesidlist.add(R.id.storie2);
        storiesidlist.add(R.id.storie3);

        storiesidlist.add(R.id.storie4);
        storiesidlist.add(R.id.storie5);
        storiesidlist.add(R.id.storie6);

        storiesidlist.add(R.id.storie7);
        storiesidlist.add(R.id.storie8);
        storiesidlist.add(R.id.storie9);

        storiesidlist.add(R.id.storie10);
        storiesidlist.add(R.id.storie11);
        storiesidlist.add(R.id.storie12);

        storiesidlist.add(R.id.storie13);
        storiesidlist.add(R.id.storie14);
        storiesidlist.add(R.id.storie15);

        storiesidlist.add(R.id.storie16);
        storiesidlist.add(R.id.storie17);
        storiesidlist.add(R.id.storie18);

        storiesidlist.add(R.id.storie19);
        storiesidlist.add(R.id.storie20);
        for (int i = 0; i < storiesidlist.size(); i++) {
            item.add((View) view.findViewById(storiesidlist.get(i)));
            txt_name_1.add((TextView) view.findViewById(storiesidlist.get(i)).findViewById(R.id.txt_name));
            circleImageView2_1.add((ImageView) view.findViewById(storiesidlist.get(i)).findViewById(R.id.circleImageView2));
            thumb_mail_1.add((ImageView) view.findViewById(storiesidlist.get(i)).findViewById(R.id.thumb_mail));
            main_layout_stores_1.add((ConstraintLayout) view.findViewById(storiesidlist.get(i)).findViewById(R.id.main_layout_stores));

        }
        for (int i = 0; i < storiesidlist.size(); i++) {
            item.get(i).setVisibility(View.GONE);
        }

    }

    public void setStories(List<SocialUserFriendsBroadcastModel> stories) {
        for (int i = 0; i < (stories.size() < 20 ? stories.size() : 20); i++) {
            databaseList.add(FirebaseDatabase.getInstance());
            myRef.add(databaseList.get(i).getReference());
            item.get(i).setVisibility(View.VISIBLE);
            txt_name_1.get(i).setText(stories.get(i).getUserID());
            int finalI = i;
            int finalI1 = i;
            item.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    myRef.get(finalI1).child("SocialBroadcast").child(stories.get(finalI1).getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            SocialBroadcastModel model = new SocialBroadcastModel();
                            Gson gson = new Gson();
                            JsonElement jsonElement = gson.toJsonTree(dataSnapshot.getValue());
                            model = gson.fromJson(jsonElement, SocialBroadcastModel.class);
                            Intent intent = new Intent(mContext, LiveRoomActivity.class);
                            intent.putExtra(ConstantApp.ACTION_KEY_CROLE, io.agora.rtc.Constants.CLIENT_ROLE_AUDIENCE);
                            intent.putExtra(ConstantApp.ACTION_KEY_ROOM_NAME, model.getChannelID());
                            intent.putExtra("Type", "SOCIAL");
                            intent.putExtra("URL", stories.get(finalI).getVideoUrl());
//                i.putExtra(ConstantApp.ACTION_KEY_ROOM_NAME, modelList.get(position).getProperty_id());
                            mActivity.startActivity(intent);

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }
            });

            RequestOptions options = new RequestOptions().centerCrop();
            Glide.with(mContext).load(stories.get(finalI).getsImageUrl()).apply(options)
                    .into(thumb_mail_1.get(i));
            myRef.get(finalI).child("SocialUsers").child(stories.get(finalI).getUserID()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    SocialUserDetailsModel model = new SocialUserDetailsModel();
                    Gson gson = new Gson();
                    JsonElement jsonElement = gson.toJsonTree(dataSnapshot.getValue());
                    model = gson.fromJson(jsonElement, SocialUserDetailsModel.class);
                    txt_name_1.get(finalI).setText(model.getUser_name());
                    RequestOptions options = new RequestOptions().centerCrop();
                    Glide.with(mContext).load(model.getImage()).apply(options)
                            .into(circleImageView2_1.get(finalI));
                    //list.add(model);

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }


}

class ProgressSpinnerMilla {

}

class MultiImages3 {
    MainDetails mainDetails;

    public void setMainDetails(MainDetails mainDetails) {
        this.mainDetails = mainDetails;
    }

    public MainDetails getMainDetails() {
        return mainDetails;
    }
}

class MultiImages5 {
    MainDetails mainDetails;

    public void setMainDetails(MainDetails mainDetails) {
        this.mainDetails = mainDetails;
    }

    public MainDetails getMainDetails() {
        return mainDetails;
    }
}

class MultiImages6 {
    MainDetails mainDetails;

    public void setMainDetails(MainDetails mainDetails) {
        this.mainDetails = mainDetails;
    }

    public MainDetails getMainDetails() {
        return mainDetails;
    }
}

class VideoOnly {
    MainDetails mainDetails;

    public void setMainDetails(MainDetails mainDetails) {
        this.mainDetails = mainDetails;
    }

    public MainDetails getMainDetails() {
        return mainDetails;
    }
}

class MultiImages1 {
    MainDetails mainDetails;

    public void setMainDetails(MainDetails mainDetails) {
        this.mainDetails = mainDetails;
    }

    public MainDetails getMainDetails() {
        return mainDetails;
    }
}

class Live2 {
    MainDetails objm1;

    LivePostCallback mLivePostCallback;
    boolean first = false;

    public Live2(LivePostCallback mLivePostCallback) {
        this.mLivePostCallback = mLivePostCallback;
        Log.i("Live2", "called");

        objm1 = new MainDetails();
        getSocialUserPostsMilla();
    }

    public void getSocialUserPostsMilla() {
        Log.i("Live2", "called1");
        String UserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        //loadfinished=false;

        Log.i("getSocialUser718", "called");
        Query imagesQuery;
        {

            imagesQuery = FirebaseDatabase.getInstance().getReference("SocialUserPosts").child(UserId)
                    .orderByKey().limitToLast(1);
        }

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null ? dataSnapshot.getChildrenCount() > 0 ? true : false : false) {
                    objm1 = new MainDetails();
                    long count = dataSnapshot.getChildrenCount();
                    Log.i("getSocialUser718", "count " + count);
                    final int[] ct = {0};
                    SocialUserPostsModel postidlist = new SocialUserPostsModel();// new ArrayList<>();

                    for (DataSnapshot data : dataSnapshot.getChildren()) {


                        Log.i("Live2", (data.getValue().toString()));

                        if (first == false) {

                            first = true;
                        } else {
                            String postid = data.getKey();
                            SocialUserPostsModel s1 = data.getValue(SocialUserPostsModel.class);
                            s1.setmSocialPostsid(postid);
                            Log.i("mMainDetailsf4", data.getKey() + "  " + postid);
                            postidlist = (s1);
                            try {
                                getSocialPostsMilla(postidlist);
                            } catch (Exception e) {
                                Log.i("Live2_e", e.getMessage() + "");
                            }

                        }
                    }


                } else {

                    Log.i("getSocialUser718", "count is zero");
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };


        imagesQuery.addValueEventListener(valueEventListener);


    }

    public void getSocialPostsMilla(SocialUserPostsModel postidlist1) {
        //List<SocialUserPostsModel> userPostsList;
        //List<SocialPostsModel> postList;
        DatabaseReference myRef;
        FirebaseDatabase mDatabase;
        mDatabase = FirebaseDatabase.getInstance();
        myRef = mDatabase.getReference();
        Log.i("Live2", "called3");

        {
            //  Log.i("mMainDetails567" + "s" + postidlist1.size(), "getSocialPosts called " + postidlist1.get(s).getmSocialPostsid());


            myRef.child("SocialPosts").child(postidlist1.getmSocialPostsid())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            //SocialPostsModel obj1=dataSnapshot.getValue(SocialPostsModel.class);
                            try {

                                objm1.setS1(true);                     //setmSocialPostsid
                                objm1.postid = dataSnapshot.getKey();// postidlist1.get(finalS).getmSocialPostsid();
                                objm1.setmSocialUserPostsModel(postidlist1);


                                objm1.mSocialPostsModel = dataSnapshot.getValue(SocialPostsModel.class);
                                objm1.mSocialPostsModel.setmSocialPostsid(postidlist1.getmSocialPostsid());
                                objm1.setSocialUserPosts(postidlist1);

                                {
//                                    Log.i("mMainDetails" + callingct, "SocialPosts size matched");
                                    getSocialUsers(postidlist1);
                                }
                            } catch (Exception e) {

                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {


                            objm1.postid = postidlist1.getmSocialPostsid();


                            {
                                getSocialUsers(postidlist1);
                            }

                            Log.e("DatabaseError", databaseError + "");
                        }
                    });


        }

    }

    public void getSocialUsers(SocialUserPostsModel postsModelList) {
        Log.i("Live2", "called4");
        Log.i("mMainDetails898", "getSocialUsers called");
        DatabaseReference myRef;
        FirebaseDatabase mDatabase;
        mDatabase = FirebaseDatabase.getInstance();
        myRef = mDatabase.getReference();

        {

            myRef.child("SocialUsers").child(postsModelList.getPostedUserID())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {


                            try {
                                objm1.mSocialUserDetailsModel = dataSnapshot.getValue(SocialUserDetailsModel.class);
                                objm1.mSocialUserDetailsModel.setPushKey(dataSnapshot.getKey());
                                objm1.setS2(true);
                            } catch (IndexOutOfBoundsException e) {

                            }


                            {


                                getSocialPostContents(postsModelList);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {


                            {
                                getSocialPostContents(postsModelList);
                            }
                        }
                    });
        }
    }

    public void getSocialPostContents(SocialUserPostsModel postsModelList) {
        Log.i("Live2", "called5");
        DatabaseReference myRef;
        FirebaseDatabase mDatabase;
        mDatabase = FirebaseDatabase.getInstance();
        myRef = mDatabase.getReference();


        Log.i("mMainDetails936", "getSocialPostContents called");

        {

            String mSocialPostContents_content_id;
            if (objm1.getmSocialPostsModel().isSharedPost()) {
                mSocialPostContents_content_id = objm1.getmSocialPostsModel().getSharedPostID();
            } else {
                mSocialPostContents_content_id = postsModelList.getmSocialPostsid();
            }

            myRef.child("SocialPostContents").child(mSocialPostContents_content_id).
                    addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {


                            try {
                                objm1.setS3(true);
                                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                    newDataBean obj1 = snapshot.getValue(newDataBean.class);
                                    obj1.setPostId(snapshot.getKey());
                                    obj1.setContentId_new(snapshot.getKey());
                                    obj1.setPushKey(snapshot.getKey());
                                    try {
                                        Log.i("Live2_r_2", "  mSocialPostContentsModelsList:" + obj1.getContentId_new() + "\n" +
                                                obj1.getPostId());
                                    } catch (Exception e) {
                                        Log.i("Live2", "called10  " + e.getMessage());
                                    }
                                    objm1.mSocialPostContentsModelsList.add(obj1);
                                }
                            } catch (IndexOutOfBoundsException e) {
                                Log.i("Live2", "called11  " + e.getMessage());
                            }


                            {
                                getSocialPostCounters(postsModelList);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            {
                                getSocialPostCounters(postsModelList);
                            }
                        }
                    });
        }
    }

    public void getSocialPostCounters(SocialUserPostsModel postsModelList) {
        Log.i("Live2", "called6");
        Log.i("mMainDetails976", "getSocialPostContents called");
        DatabaseReference myRef;
        FirebaseDatabase mDatabase;
        mDatabase = FirebaseDatabase.getInstance();
        myRef = mDatabase.getReference();
        Log.i("milla", "getSocialPostCounters called ");//+ct[0]);

        {

            myRef.child("SocialPostCounters").child(postsModelList.getmSocialPostsid()).addListenerForSingleValueEvent(
                    new ValueEventListener() {

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {


                            {

                                // getSocialUserPostsMilla_key=postsModelList.get(0).getmSocialPostsid();
                                objm1.mSocialPostCountersModel = dataSnapshot.getValue(SocialPostCountersModel.class);

                                objm1.setS4(true);

                                {
                                    getSocialPostReactions();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                            {
                                getSocialPostReactions();
                            }
                        }
                    });
        }
    }

    public void getSocialPostReactions() {
        Log.i("Live2", "called7");

        mLivePostCallback.onNewPost(objm1);

        String s1 = "nil";//new Gson().toJson(objm1,MainDetails.class);
        try {
            Log.i("Live2_r1_1", s1 + "  getContentId_new:" + objm1.getPostid() + ":" + objm1.getmSocialPostContentsModelsList().get(0).getContentId_new());

        } catch (Exception e) {
            Log.i("Live2", "called12  " + e.getMessage());
        }
        try {

            Log.i("Live2_r1_2", objm1.getPostid() + ":" + objm1.getmSocialPostContentsModelsList().get(0).getPostId() + "");
        } catch (Exception e) {
            Log.i("Live2", "called13  " + e.getMessage());
        }

        Log.i("Live2", "called8");
        //   loadfinished=true;
    }
}